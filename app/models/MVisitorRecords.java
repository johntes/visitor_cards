package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Constraint;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class MVisitorRecords  extends Model{
    @Id
    public long aid;

    @Constraints.Required
    public String FullName;

    @Constraints.Required
    public String Email;

    @Constraints.Required
    public String Country;

    @Constraints.Required
    public String NationalId;

    @Constraints.Required
    public String Address;

    @Constraints.Required
    public String Company;


    @Constraints.Required
    public String Gender;

    @Constraints.Required
    public String Languages;

    @Constraints.Required
    public String Image;

    @Constraints.Required
    public String phoneNumber;

    @Constraints.Required
    public int NoOfTimes;

    public static Model.Finder<Long, MVisitorRecords> findVisitorsRecords = new Model.Finder<Long, MVisitorRecords>(Long.class, MVisitorRecords.class);
    public static List<MVisitorRecords> findAll() { return findVisitorsRecords.where().findList();}





    public static MVisitorRecords findVisitorById(String id){
        return MVisitorRecords.findVisitorsRecords.where().eq("NationalId",id)
                .findUnique();
    }

    public static List<MVisitorRecords> findRecurrentVistorList(String Id){
        return findVisitorsRecords.where().eq("NationalId",Id).findList();
    }
}
