package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class MPremise extends Model{
    @Id public long aid;

    @Constraints.Required
    public String premiseName;




    public static Model.Finder<Long, MPremise> findPremise = new Model.Finder<Long, MPremise>(Long.class,  MPremise.class);

    public static List<MPremise> findAll() {
        return findPremise.where().findList();
    }


    /*public static Map<String, String> User() {
        LinkedHashMap<String,String> options=new LinkedHashMap<>();
        for(MCards c: MCards.findUsers.
                where().eq("isActive","1")
                .orderBy("UserName").findList()){
            options.put(Long.toString(c.aid),c.UserName );
        }
        return options;
    }*/




    public static MPremise findPremiseByName(String premiseName){
        return MPremise.findPremise.where().eq("premiseName",premiseName)
                .findUnique();
    }


}
