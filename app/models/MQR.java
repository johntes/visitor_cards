package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class MQR extends Model{
    @Id public long aid;

    @Constraints.Required
    public String VisitorId;

    //this being assigned status
    @Constraints.Required
    public String isActive;

    //has admin activated the card
    @Constraints.Required
    public String time_generated;

    @Constraints.Required
    public String generated_by;


    @Constraints.Required
    public String QrImage;










    public static Model.Finder<Long, MQR> findQr = new Model.Finder<Long, MQR>(Long.class,  MQR.class);

    public static List<MQR> findAll() { return findQr.where().findList();
    }
    public static List<MQR> findActiveUsers() { return findQr.where().eq("isActive", "1").findList();
    }





    public static MQR findActiveQr(String idNumber){
        return MQR.findQr.where().eq("VisitorId",idNumber).eq("isActive","1")
                .findUnique();
    }

    public static MQR findUnavailableCard(String cardNumber){
        return MQR.findQr.where().eq("cardNumber",cardNumber).eq("isActive","1").eq("admin_activated","0")
                .findUnique();
    }


    public static MQR exitQr(String VisitorId){
        return MQR.findQr.where().eq("VisitorId",VisitorId).eq("isActive","1")
                .findUnique();
    }





}
