package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class MEventVisitor extends Model{
    @Id
    public long aid;

    @Constraints.Required
    public String FullName;

    @Constraints.Required
    public String Email;

    @Constraints.Required
    public String PhoneNumber;

    @Constraints.Required
    public String Organisation;

    @Constraints.Required
    public String Confirmation;

    @Constraints.Required
    public String Designation;

    @Constraints.Required
    public String eventAid;

    @Constraints.Required
    public String Attended;

    @Constraints.Required
    public String Expected;

    @Constraints.Required
    public String Country;

    @Constraints.Required
    public String NationalId;

    @Constraints.Required
    public String Address;


    @Constraints.Required
    public String Gender;

    @Constraints.Required
    public String Languages;


    @Constraints.Required
    public String cardImage;

    @Constraints.Required
    public String timeIn;




    public static Model.Finder<Long, MEventVisitor> findEventVisitors = new Model.Finder<Long, MEventVisitor>(Long.class, MEventVisitor.class);
    public static List<MEventVisitor> findAll() { return findEventVisitors.where().findList();}



    public static MEventVisitor findVisitorByEmail(String Email){
        return MEventVisitor.findEventVisitors.where().eq("Email",Email)
                .findUnique();
    }

    public static MEventVisitor findVisitorByAid(String aid){
        return MEventVisitor.findEventVisitors.where().eq("aid",aid)
                .findUnique();
    }


    public static MEventVisitor findVisitorByEmailAndEvent(String Email,String eventAid){
        return MEventVisitor.findEventVisitors.where().eq("Email",Email).eq("eventAid",eventAid)
                .findUnique();
    }

    public static MEventVisitor findCheckedInVisitorByEmail(String Email){
        return MEventVisitor.findEventVisitors.where().eq("Email",Email).eq("Attended","1")
                .findUnique();
    }



    public static List<MEventVisitor> findRecurrentVistorList(String Id){
        return findEventVisitors.where().eq("NationalId",Id).findList();
    }


    public static List<MEventVisitor> findEventVistorList(String EventName){
        return findEventVisitors.where().eq("eventAid",EventName).findList();
    }

    public static List<MEventVisitor> findUncheckedVistorList(String eventAid){
        return findEventVisitors.where().eq("eventAid",eventAid).eq("Expected","yes").eq("Attended","0").findList();
    }

}
