package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class MCards extends Model{
    @Id public long aid;

    @Constraints.Required
    public String cardNumber;

    //this being assigned status
    @Constraints.Required
    public String isActive;

    //has admin activated the card
    @Constraints.Required
    public String admin_activated;

    









    public static Model.Finder<Long, MCards> findCards = new Model.Finder<Long, MCards>(Long.class,  MCards.class);

    public static List<MCards> findAll() {
        return findCards.where().findList();
    }
    public static List<MCards> findActiveUsers() { return findCards.where().eq("isActive", "1").findList();
    }

    /*public static Map<String, String> User() {
        LinkedHashMap<String,String> options=new LinkedHashMap<>();
        for(MCards c: MCards.findUsers.
                where().eq("isActive","1")
                .orderBy("UserName").findList()){
            options.put(Long.toString(c.aid),c.UserName );
        }
        return options;
    }*/

    public static MCards authenticateMobileUser(String id, String pwd){
        return MCards.findCards.where().eq("cardNumber",id).
                eq("Password",pwd).eq("UserType","mobile").eq("isActive","1")
                .findUnique();
    }


    public static MCards authenticateWebUser(String id, String pwd){
        return MCards.findCards.where().eq("cardNumber",id).
                eq("Password",pwd).eq("UserType","web").eq("isActive","1")
                .findUnique();
    }

    public static MCards findAvailableCard(String cardNumber){
        return MCards.findCards.where().eq("cardNumber",cardNumber).eq("isActive","0").eq("admin_activated","1")
                .findUnique();
    }

    public static MCards findUnavailableCard(String cardNumber){
        return MCards.findCards.where().eq("cardNumber",cardNumber).eq("isActive","1").eq("admin_activated","0")
                .findUnique();
    }

    public static MCards findCardExist(String cardNumber){
        return MCards.findCards.where().eq("cardNumber",cardNumber)
                .findUnique();
    }


    public static MCards exitCard(String cardNumber){
        return MCards.findCards.where().eq("cardNumber",cardNumber)
                .findUnique();
    }





}
