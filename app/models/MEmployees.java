package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class MEmployees extends Model{
    @Id public long aid;

    @Constraints.Required
    public String FullName;

    @Constraints.Required
    public String Email;

    @Constraints.Required
    public String IdNumber;

    @Constraints.Required
    public String Department;

    @Constraints.Required
    public String EmploymentNumber;

    @Constraints.Required
    public String PhoneNumber;

    @Constraints.Required
    public String password;


    @Constraints.Required
    public String password_set;

    @Constraints.Required
    public String isActive;

    public static Model.Finder<Long, MEmployees> findEmployees = new Model.Finder<Long, MEmployees>(Long.class,  MEmployees.class);

    public static List<MEmployees> findAll() { return findEmployees.where().findList();
    }

    public static List<MEmployees> findActiveEmployees() {
        return findEmployees.where().eq("isActive", "1").findList();
    }

    public static Map<String, String> Employees() {
        LinkedHashMap<String,String> options=new LinkedHashMap<>();
        for(MEmployees c: MEmployees.findEmployees.
                where().eq("isActive","1")
                .orderBy("FullName").findList()){
            options.put(Long.toString(c.aid),c.FullName );
        }
        return options;
    }

    public static MEmployees findEmployeeByAid(String aid){
        return MEmployees.findEmployees.where().eq("aid",aid).
                findUnique();
    }

    public static MEmployees findEmployee(String id){
        return MEmployees.findEmployees.where().eq("IdNumber",id).
                findUnique();
    }

    public static MEmployees findActiveEmployee(String EmploymentNumber){
        return MEmployees.findEmployees.where().eq("EmploymentNumber",EmploymentNumber).eq("isActive","1").
                findUnique();
    }

    public static MEmployees findEmployeeNumber(String employmentNumber){
        return MEmployees.findEmployees.where().eq("EmploymentNumber",employmentNumber).
                findUnique();
    }

    public static MEmployees authenticateEmployee(String username,String password){
        return MEmployees.findEmployees.where().eq("Email",username).eq("IdNumber",password).
                findUnique();
    }


    public static MEmployees passwordAuthentication(String email,String password){
        return MEmployees.findEmployees.where().eq("Email",email).eq("password",password).
                findUnique();
    }




}





