package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class MExpectedVisitors extends Model{
    @Id
    public long aid;

    @Constraints.Required
    public String FullName;

    @Constraints.Required
    public String Email;

    @Constraints.Required
    public String Country;

    @Constraints.Required
    public String NationalId;

    @Constraints.Required
    public String Address;

    @Constraints.Required
    public String Company;

    @Constraints.Required
    public String HostName;


    @Constraints.Required
    public String Gender;

    @Constraints.Required
    public String Languages;

    @Constraints.Required
    public String phoneNumber;

    @Constraints.Required
    public String Date;

    @Constraints.Required
    public String visitType;

    @Constraints.Required
    public String employeeId;

    @Constraints.Required
    public String isActive;


    @Constraints.Required
    public String time;

    @Constraints.Required
    public String vip;


    public static Model.Finder<Long, MExpectedVisitors> findExpectedVisitorsRecords = new Model.Finder<Long, MExpectedVisitors>(Long.class, MExpectedVisitors.class);
    public static List<MExpectedVisitors> findAll() { return findExpectedVisitorsRecords.where().findList();}





    public static MExpectedVisitors findVisitorById(String id){
        return MExpectedVisitors.findExpectedVisitorsRecords.where().eq("NationalId",id)
                .findUnique();
    }

    public static MExpectedVisitors findActiveVisitorById(String id){
        return MExpectedVisitors.findExpectedVisitorsRecords.where().eq("NationalId",id).eq("isActive","1")
                .findUnique();
    }

    public static List<MExpectedVisitors> findActiveExpctedVisitorList(){
        return findExpectedVisitorsRecords.where().eq("isActive","1").findList();
    }

    public static List<MExpectedVisitors> findYourVistorList(String Id){
        return findExpectedVisitorsRecords.where().eq("employeeId",Id).eq("isActive","1").findList();
    }
}
