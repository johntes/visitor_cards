package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class MEvents extends Model{
    @Id public long aid;

    @Constraints.Required
    public String eventName;

    @Constraints.Required
    public String eventDate;


    @Constraints.Required
    public String eventStatus;

    @Constraints.Required
    public String eventVenue;

    @Constraints.Required
    public String eventCheckOutTime;




    public static Model.Finder<Long, MEvents> findEvents = new Model.Finder<Long, MEvents>(Long.class,  MEvents.class);

    public static List<MEvents> findAll() { return findEvents.where().findList();
    }
    public static List<MEvents> findActiveEvents() { return findEvents.where().eq("eventStatus", "active").findList();
    }
    public static List<MEvents> findEndedEvents() { return findEvents.where().eq("eventStatus", "ended").findList();
    }

    public static List<MEvents> findOnComingEvents() { return findEvents.where().eq("eventStatus", "oncoming").findList();
    }
    public static List<MEvents> findOnComingAndOngoingEvents() { return findEvents.where().eq("eventStatus", "oncoming").eq("eventStatus", "active").findList();
    }

    public static MEvents findEventByEventAid(String eventAid){
        return MEvents.findEvents.where().eq("aid",eventAid)
                .findUnique();
    }

    public static MEvents findEventByEventName(String eventName){
        return MEvents.findEvents.where().eq("eventName",eventName)
                .findUnique();
    }

    public static MEvents findEndedEventByEventAid(String eventAid){
        return MEvents.findEvents.where().eq("aid",eventAid).eq("eventStatus","ended")
                .findUnique();
    }

    public static MEvents findStartedEventByEventAid(String eventAid){
        return MEvents.findEvents.where().eq("aid",eventAid).eq("eventStatus","active")
                .findUnique();
    }

    public static MEvents findEventById(Long id){
        return MEvents.findEvents.where().eq("aid",id)
                .findUnique();
    }



    public static Map<String, String> MEvents() {
        LinkedHashMap<String,String> options=new LinkedHashMap<>();
        for(MEvents c: MEvents.findEvents.
                where().eq("eventStatus","oncoming").eq("eventStatus","active")
                .orderBy("eventDate").findList()){
            options.put(Long.toString(c.aid),c.eventName );
        }
        return options;
    }


}
