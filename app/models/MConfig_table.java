package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class MConfig_table extends Model{
    @Id public long aid;

    @Constraints.Required
    public String systemType;

    //this being assigned status










    public static Model.Finder<Long, MConfig_table> findConfig = new Model.Finder<Long, MConfig_table>(Long.class,  MConfig_table.class);

    public static List<MConfig_table> findAll() { return findConfig.where().findList();
    }
    public static List<MConfig_table> findActiveUsers() { return findConfig.where().eq("isActive", "1").findList();
    }



    public static MConfig_table findConfig(String systemType){
        return MConfig_table.findConfig.where().eq("systemType",systemType)
                .findUnique();
    }




}
