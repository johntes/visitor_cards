package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Constraint;
import java.sql.Date;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class MVisitors extends Model {

    @Id
    public long aid;

    @Constraints.Required
    public String FullName;

    @Constraints.Required
    public String Email;

    @Constraints.Required
    public String phoneNumber;

    @Constraints.Required
    public String Country;

    @Constraints.Required
    public String NationalId;

    @Constraints.Required
    public String Company;

    @Constraints.Required
    public String Type;

    @Constraints.Required
    public String Host;

    @Constraints.Required
    public String Gender;

    @Constraints.Required
    public String TimeIn;

    @Constraints.Required
    public String TimeOut;

    @Constraints.Required
    public String Languages;

    @Constraints.Required
    public String Date;

    @Constraints.Required
    public String Image;

    @Constraints.Required
    public String isActive;

    @Constraints.Required
    public String cardNumber;

    @Constraints.Required
    public String isDeleted;

    @Constraints.Required
    public String QrImage;

    @Constraints.Required
    public String employeeAid;

    /*@Constraints.Required
    public java.sql.Date date;*/


    public static Model.Finder<Long, MVisitors> findVisitors = new Model.Finder<Long, MVisitors>(Long.class, MVisitors.class);

    public static List<MVisitors> findAll() {
        return findVisitors.where().findList();
    }

    public static List<MVisitors> findActiveVisitors() {
        return findVisitors.where().eq("isActive", "1").findList();
    }

    public static List<MVisitors> findCurrentVisitors() {
        return findVisitors.where().eq("isActive", "1").findList();
    }

    public static List<MVisitors> findExitedVisitors() {
        return findVisitors.where().eq("isActive", "0").findList();
    }

    public static MVisitors checkVisitor(String cardNumber, String isActive){
        return MVisitors.findVisitors.where().eq("cardNumber",cardNumber).
                eq("isActive",isActive)
                .findUnique();
    }

    public static MVisitors checkVisitorQr(String NationalId){
        return MVisitors.findVisitors.where().eq("NationalId",NationalId).
                eq("isActive","1")
                .findUnique();
    }

    public static MVisitors checkVisitorStatus(String NationalId){
        return MVisitors.findVisitors.where().eq("NationalId",NationalId).
                eq("isActive","1")
                .findUnique();
    }


    public static MVisitors findHostByEmployeeAid(String empAid){
        return MVisitors.findVisitors.where().eq("employeeAid",empAid).
                eq("isActive","1")
                .findUnique();
    }




    public static List<MVisitors> findDiActivetedVisitors()
    {
        return findVisitors.where().eq("isActive", "0").findList();
    }

    public static MVisitors findVisitorById(String id){
        return MVisitors.findVisitors.where().eq("NationalId",id)
                .findUnique();
    }

    public static List<MVisitors> findRecurrentVistorList(String Id){
        return findVisitors.where().eq("NationalId",Id).findList();
    }

    /*public static List<MVisitors> findVisitors(String Id){
        return findVisitors.where().eq("NationalId",Id).findList();
    }*/





}
