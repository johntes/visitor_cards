package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.*;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Cards.*;
import views.html.Users.*;


import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

import static controllers.CUsers.isLoggedIn;
import static controllers.EncryptionTest.encrypt;

public class CCards extends Controller{

    public static Result RenderAddCard() {
        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(AddCards.render("Add cards"));
    }


    public static Result RenderViewCard() {
        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(ViewCards.render("View Card"));
    }

    public static Result RenderImport() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(importCsv.render("import csv"));
    }



    public static Result ActivateCard(Long id){
        MCards mUsers = new MCards();
        mUsers.aid=id;
        mUsers.admin_activated="1";
        mUsers.update();
        return redirect(routes.CCards.RenderViewCard());
    }

    public static Result DeActivateCard(Long id){
        MCards mUsers = new MCards();
        mUsers.aid=id;
        mUsers.admin_activated="0";
        mUsers.update();

        return redirect(routes.CCards.RenderViewCard());
    }


    public static Result ReceiveCard() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String cardNumber = requestform.get("cardNumber");
        System.out.println("UserName: "+cardNumber);
        return ok();
    }


    public static Result addCardFromCsv() throws IOException {

        Http.MultipartFormData body = request().body().asMultipartFormData();
        if(body == null)
        {
            return badRequest("Invalid request, required is POST with enctype=multipart/form-data.");
        }

        Http.MultipartFormData.FilePart filePart = body.getFile("cardFile");
        if(filePart == null)
        {
            return badRequest("Invalid request, no file has been sent.");
        }

        // getContentType can return null, so we check the other way around to prevent null exception
       /* if(!"application/pdf".equalsIgnoreCase(filePart.getContentType()))
        {
            return badRequest("Invalid request, only PDFs are allowed.");
        }*/

        File file = filePart.getFile();


        List<Book> books =CSVReaderInJava.readBooksFromCSV(file);
        System.out.print("name"+"\t");
        System.out.print("price"+"\t");
        System.out.println("Author"+"\t");
        System.out.println("......................................................................");
        // let's print all the person read from CSV file
        int iteration=0;
int len=MCards.findAll().size();
int len2=0;
        for (Book b : books) {
            if(iteration == 0) {

                String index=b.getIndex();
                String cardNumber=b.getCardNumber();



                iteration++;
                continue;
            }

            System.out.print(iteration+"\t");
            String index=b.getIndex();
            String cardNumber=b.getCardNumber();
            System.out.print(index+"\t");
            System.out.print(b.getCardNumber()+"\t");

            System.out.println("");


    if (MCards.findCardExist(cardNumber) == null) {
        MCards mCards = new MCards();
        mCards.cardNumber = cardNumber;
        mCards.isActive = "0";
        mCards.admin_activated = "0";
        mCards.save();
        len2 = MCards.findAll().size();
        iteration++;
    }
        }
        if(len2>len){
            flash("typesuccess", "successfully added unavailable cards from csv");
        }
        else {
            flash("typeerror", "failed to add");
        }

        System.out.print(iteration+" this is the iterations");

        return redirect(routes.CCards.RenderViewCard());


    }




    public static Result AddCard() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String cardNumber = requestform.get("cardNumber");
        MCards mCards = new MCards();
        if (MCards.findCardExist(cardNumber) == null) {
            mCards.cardNumber = cardNumber;
            mCards.isActive = "0";
            mCards.admin_activated = "0";
            mCards.save();
            flash("success", "card added successfully");
            return redirect(routes.CCards.RenderViewCard());
        }
        else
            flash("typeerror", "Card Already Exists");
            return redirect(routes.CCards.RenderViewCard());
        }



        public static Result cardList () {
            Map<String, String[]> params = request().queryString();
            Integer iTotalRecords = MCards.findCards.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


            String sortBy = "cardNumber";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "cardNumber";
                    break;
                case 1:
                    sortBy = "aid";
                    break;
                case 2:
                    sortBy = "admin_activated";
                    break;
            }

            Page<MCards> areaPage = MCards.findCards.where(
                    Expr.or(
                            Expr.ilike("cardNumber", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("isActive", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();
            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MCards cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                row.put("aid", cc.aid);
                row.put("cardNumber", cc.cardNumber);
                row.put("admin_activated", cc.admin_activated);
                String assigned=cc.isActive;
                if(assigned.equals("1"))
                {
                    row.put("assigned","yes");
                }
                else
                {
                    row.put("assigned","no");
                }


                anc.add(row);
            }

            return ok(result);

        }



    }
