package controllers;
import models.MEventVisitor;
import models.MEvents;
import models.MUsers;
import play.*;
import play.mvc.*;
import play.mvc.Result;
import play.libs.Json;
import play.mvc.Controller;
//import org.Codehaus.jackson.node.ObjectNode;
import  com.avaje.ebean.Ebean;
import  com.avaje.ebean.Expr;
import  com.avaje.ebean.Page;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.Logger;
//import play.DynamicForm;
import play.data.Form;

import play.data.DynamicForm;

import java.util.ArrayList;
import java.util.List;
//import com.innovatrics.mrz.types.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static controllers.CDashboard.isEventLoggedIn;

public class EventMobileApi extends Controller{

    public static Result authenticateEventsMobileUsers() {
        System.out.println("Reaching the server");
        DynamicForm requestform = Form.form().bindFromRequest();
        String Usernem = requestform.get("username");
        String Password = requestform.get("pass");

        System.out.println("username: "+Usernem);
        System.out.println("Password: "+Password);

        ObjectNode objectNode;
        objectNode = Json.newObject();

        MUsers mUsers=MUsers.authenticateEventMobileUser(Usernem, Encryption.encryptString(Password));

        if (mUsers != null) {
            objectNode.put("responseCode", "200");
            return ok(objectNode);

        } else {
            objectNode.put("responseCode", "201");
            return ok(objectNode);
        }

    }


    public static Result return_events() {
        System.out.println("Reaching on the server");
        return ok(Json.toJson(MEvents.findAll()));
    }


    public static Result return_event_visitors() {
        System.out.println("Reaching the return event visitor endpoint");
        DynamicForm requestform = Form.form().bindFromRequest();
        String eventAid = requestform.get("eventAid");
        return ok(Json.toJson(MEventVisitor.findUncheckedVistorList(eventAid)));
    }

    //checking in visitors
    public static Result ReceiveAttendanceEventVisitors(){
        ObjectNode objectNode;
        objectNode = Json.newObject();
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String PhoneNumber = requestform.get("PhoneNumber");
        String Company = requestform.get("Company");
        String Confirmation = "yes";
        String Designation = requestform.get("Designation");
        String eventAid = requestform.get("eventAid");
        String cardImage=requestform.get("cardImage");
        String timeIn=requestform.get("timeIn");


        System.out.println("the Email name is "+Email);
        System.out.println("the timeIn name is "+timeIn);
        System.out.println("the Designation name is "+Designation);
        System.out.println("the eventAid is "+eventAid);
        System.out.println("the eventAid is "+eventAid);



         if(MEventVisitor.findCheckedInVisitorByEmail(Email)!=null){
           //visitor was already checked in
             objectNode.put("responseCode", "201");

        }
      else  if(MEventVisitor.findVisitorByEmail(Email)!=null)
        {

            //the visitor already exist in records so we update the record by aid
            Long aid=MEventVisitor.findVisitorByEmail(Email).aid;
            MEventVisitor mEventVisitor = new MEventVisitor();
            mEventVisitor.Designation = Designation;
            mEventVisitor.Organisation=Company;
            mEventVisitor.eventAid = eventAid;
            mEventVisitor.Expected = "yes";
            mEventVisitor.Attended = "1";
            mEventVisitor.cardImage=cardImage;
            mEventVisitor.timeIn=MobileApi.getTime();
            mEventVisitor.update(aid);

            objectNode.put("responseCode", "200");
            return ok(objectNode);
            //objectNode.put("responseCode", "204");
        }

        else if(MEvents.findEndedEventByEventAid(eventAid)!=null)
         {
             //event has ended so cant register
             objectNode.put("responseCode", "203");
         }
        else {

             if(MEvents.findStartedEventByEventAid(eventAid)==null && MEvents.findEndedEventByEventAid(eventAid)==null)
             {
                 Long aid=MEvents.findEventByEventAid(eventAid).aid;
                 MEvents mEvents=new MEvents();
                 mEvents.eventStatus="active";
                 mEvents.update(aid);
             }

            MEventVisitor mEventVisitor = new MEventVisitor();
            mEventVisitor.Organisation = Company;
            mEventVisitor.Email = Email;
            mEventVisitor.Designation = Designation;
            mEventVisitor.Confirmation = Confirmation;
            mEventVisitor.PhoneNumber = PhoneNumber;
            mEventVisitor.FullName = FullName;
            mEventVisitor.eventAid=eventAid;
            mEventVisitor.Expected="no";
            mEventVisitor.Attended="1";
            mEventVisitor.cardImage=cardImage;
            mEventVisitor.timeIn=MobileApi.getTime();
            mEventVisitor.save();
             objectNode.put("responseCode", "200");
        }

        return ok(objectNode);
    }


    public static Result check_out_Event() {
        System.out.println("Reaching the check in  visitor endpoint");
        DynamicForm requestform = Form.form().bindFromRequest();
        ObjectNode objectNode;
        objectNode = Json.newObject();
        String eventAid = requestform.get("eventAid");
        String eventCheckOutTime=requestform.get("eventCheckOutTime");

        if(MEvents.findEventByEventAid(eventAid)==null)
        {
            //no such event
            objectNode.put("responseCode","203");
        }

        if(MEvents.findEndedEventByEventAid(eventAid)!=null)
        {
            //it has ended
            objectNode.put("responseCode","201");
        }

        else{
            Long aid=MEvents.findEventByEventAid(eventAid).aid;
            MEvents mEvents= new MEvents();
            mEvents.eventStatus="ended";
            mEvents.eventCheckOutTime=MobileApi.getTime();
            mEvents.update(aid);
            objectNode.put("responseCode","200");
        }

       return ok(objectNode);
    }


//prebooked visitors
    public static Result check_in_visitors() {
        System.out.println("Checking in existing visitors");
        DynamicForm requestform = Form.form().bindFromRequest();
        ObjectNode objectNode;
        objectNode = Json.newObject();

        String attendeeAid="";
        String Email = requestform.get("Email");
        String Designation = requestform.get("Designation");
        String eventAid = requestform.get("eventAid");
        String cardImage=requestform.get("cardImage");
        String timeIn=requestform.get("timeIn");
        String company=requestform.get("Company");
        String phone=requestform.get("PhoneNumber");
         attendeeAid=requestform.get("attendeeAid");

        System.out.println("the Email name is "+Email);
        System.out.println("the timeIn name is "+timeIn);
        System.out.println("the Designation name is "+Designation);
        System.out.println("the eventAid is "+eventAid);
        System.out.println("the attendee aid is "+attendeeAid);

        if(MEventVisitor.findVisitorByEmail(Email)==null)
        {
            //email does not exist but the visitor exists by id
              if(MEventVisitor.findVisitorByAid(attendeeAid)!=null)
              {
                long Aid=MEventVisitor.findVisitorByAid(attendeeAid).aid;
                MEventVisitor mEventVisitor2=new MEventVisitor();
                  mEventVisitor2.Designation = Designation;
                  mEventVisitor2.Organisation=company;
                  mEventVisitor2.eventAid = eventAid;
                  mEventVisitor2.Expected = "yes";
                  mEventVisitor2.Attended = "1";
                  mEventVisitor2.PhoneNumber=phone;
                  mEventVisitor2.cardImage=cardImage;
                  mEventVisitor2.timeIn=MobileApi.getTime();
                  mEventVisitor2.Email=Email;
                  mEventVisitor2.update(Aid);

                  objectNode.put("responseCode", "200");
                  return ok(objectNode);

              }
              else
              {
                  //not preebooked
                  objectNode.put("responseCode", "201");
                  return ok(objectNode);
              }

        }

        else if(MEventVisitor.findCheckedInVisitorByEmail(Email)!=null)
        {
            //has been checked in
            objectNode.put("responseCode", "202");
            return ok(objectNode);
        }
        else {
            //updating the event to active
            if(MEvents.findStartedEventByEventAid(eventAid)==null && MEvents.findEndedEventByEventAid(eventAid)==null)
            {
             Long aid=MEvents.findEventByEventAid(eventAid).aid;
             MEvents mEvents=new MEvents();
             mEvents.eventStatus="active";
             mEvents.update(aid);
            }

            Long aid=MEventVisitor.findVisitorByEmail(Email).aid;
            MEventVisitor mEventVisitor = new MEventVisitor();
            mEventVisitor.Designation = Designation;
            mEventVisitor.Organisation=company;
            mEventVisitor.eventAid = eventAid;
            mEventVisitor.Expected = "yes";
            mEventVisitor.Attended = "1";
            mEventVisitor.PhoneNumber=phone;
            mEventVisitor.cardImage=cardImage;
            mEventVisitor.timeIn=MobileApi.getTime();
            mEventVisitor.update(aid);
            objectNode.put("responseCode", "200");
            return ok(objectNode);
        }
    }



}
