package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.MCards;
import models.MPremise;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static controllers.CUsers.isLoggedIn;

public class CPremise extends Controller{

    public static Result RenderAddPremise() {
        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(views.html.premise.AddPremises.render("Add premises"));
    }


    public static Result RenderViewPremise() {
        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(views.html.premise.ViewPremises.render("view premises"));
    }



    public static Result AddPremise() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String premiseName = requestform.get("premise");
        MPremise mPremise=new MPremise();
        if (MPremise.findPremiseByName(premiseName) == null) {
            mPremise.premiseName=premiseName;
            mPremise.save();
            flash("success", "premise added successfully");
            return redirect(routes.CPremise.RenderViewPremise());
        }
        else
            flash("typeerror", "Premise Already Exists");
            return redirect(routes.CPremise.RenderViewPremise());
        }



        public static Result premiseList () {
            Map<String, String[]> params = request().queryString();
            Integer iTotalRecords = MPremise.findPremise.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


            String sortBy = "premiseName";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "premiseName";
                    break;
                case 1:
                    sortBy = "aid";
                    break;

            }

            Page<MPremise> areaPage = MPremise.findPremise.where(
                    Expr.or(
                            Expr.ilike("premiseName", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("premiseName", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();
            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MPremise cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                row.put("aid", cc.aid);
                row.put("premiseName", cc.premiseName);

                anc.add(row);
            }

            return ok(result);

        }



    }
