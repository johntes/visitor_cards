package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;

import models.*;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Qr.ViewQr;
import views.html.Qr.DisplayQr;
import views.html.Visitors.DisplayImage;

import java.util.Map;

import static controllers.CUsers.isLoggedIn;

public class CQr extends Controller{




    public static Result RenderViewQr() {
        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewQr.render("View Qr"));
    }



    public static Result ActivateQr(Long id){
        MCards mUsers = new MCards();
        mUsers.aid=id;
        mUsers.admin_activated="1";
        mUsers.update();
        return redirect(routes.CCards.RenderViewCard());
    }


    public static Result DeActivateQr(Long id){
        MCards mUsers = new MCards();
        mUsers.aid=id;
        mUsers.admin_activated="0";
        mUsers.update();

        return redirect(routes.CCards.RenderViewCard());
    }


    public static Result ReceiveQr() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String cardNumber = requestform.get("cardNumber");
        System.out.println("UserName: "+cardNumber);
        return ok();
    }

    public static Result RenderDisplayQr(Long id){
        MQR mqr=MQR.findQr.byId(id);
        return ok(DisplayQr.render("Qr image",mqr));

    }



    public static Result AddQr() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String cardNumber = requestform.get("cardNumber");
        MCards mCards = new MCards();
        mCards.cardNumber = cardNumber;
        mCards.isActive = "0";
        mCards.admin_activated = "0";
        mCards.save();
        return redirect(routes.CCards.RenderViewCard());

        }



        public static Result QrList () {
            Map<String, String[]> params = request().queryString();
            Integer iTotalRecords = MQR.findQr.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


            String sortBy = "time_generated";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "time_generated";
                    break;
                case 1:
                    sortBy = "aid";
                    break;
                case 2:
                    sortBy = "VisitorId";
                    break;
            }

            Page<MQR> areaPage = MQR.findQr.where(
                    Expr.or(
                            Expr.ilike("time_generated", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("VisitorId", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();
            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MQR cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                row.put("aid", cc.aid);
                row.put("VisitorId", cc.VisitorId);
                row.put("time_generated", cc.time_generated);
                row.put("generated_by", cc.generated_by);
                //row.put("QrImage", cc.QrImage);


                String assigned=cc.isActive;
                if(assigned.equals("1"))
                {
                    row.put("assigned","yes");
                }
                else
                {
                    row.put("assigned","no");
                }


                anc.add(row);
            }

            return ok(result);

        }



    }
