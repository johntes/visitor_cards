package controllers;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple Java program to read CSV file in Java. In this program we will read
 * list of books stored in CSV file as comma separated values.
 *
 * @author WINDOWS 8
 *
 */
public class CSVReaderInJava {



    public static List<Book> readBooksFromCSV(File file) throws IOException {
        List<Book> books = new ArrayList<>();
        //Path pathToFile = Paths.get(fileName);

        // create an instance of BufferedReader
        // using try with resource, Java 7 feature to close resources

        final BufferedReader br = new BufferedReader(new FileReader(file));


            // read the first line from the text file
            String line = br.readLine();

            // loop until all lines are read
        int iteration=0;
            while (line != null) {

                // use string.split to load a string array with the values from
                // each line of
                // the file, using a comma as the delimiter
//ignoring the first line
                if(iteration == 0) {
                    iteration++;
                    continue;
                }

                String[] attributes = line.split(",");

                Book book = createBook(attributes);

                // adding book into ArrayList
                books.add(book);

                // read next line before looping
                // if end of file reached, line would be null
                line = br.readLine();
            }




        return books;
    }

    public static Book createBook(String[] metadata) {
        String index = metadata[0];
        String cardNumber = metadata[1];


        // create and return book of this metadata
        return new Book(index, cardNumber);
    }

}

class Book {
    private String index;
    private String cardNumber;


    public Book(String index, String cardNumber) {
        this.index = index;
        this.cardNumber = cardNumber;

    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public String toString() {
        return "Book [name=" + cardNumber + ", index=" + index
                + "]";
    }

}