package controllers;


import models.*;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Visitors.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static controllers.CDashboard.isLoggedIn;

public class CVisitors extends Controller {

    public static Result RenderViewVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewVisitors.render("Visitors")); }

    public static Result RenderViewReccurrentVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewRecurringVisitors.render("Recurring Visitors"));
    }

    public static Result RenderViewExpectedVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewrExpectedVisitors.render("Expected Visitors"));
    }

    public static Result RenderViewImage(Long id){
        MVisitors mVisitorRecords=MVisitors.findVisitors.byId(id);
        return ok(DisplayImage.render("mvisitor records",mVisitorRecords));
    }



    public static Result RenderViewExpectedDetails(Long id){
        MExpectedVisitors mExpectedVisitors=MExpectedVisitors.findExpectedVisitorsRecords.byId(id);
        return ok(DisplayExpectedVisitorDetails.render("expected visitors",mExpectedVisitors));
    }



    public static Result RenderViewImageRecurring(Long id){
        MVisitorRecords mVisitorRecords=MVisitorRecords.findVisitorsRecords.byId(id);
        return ok(DisplayImageRecurring.render("mvisitor records",mVisitorRecords));

    }

    public static Result RenderViewYourVisitorsDetails(Long id){
        MVisitors mVisitors=MVisitors.findVisitors.byId(id);
        return ok(views.html.Visitors.DisplayYourVisitors.render("Your visitors Details",mVisitors));

    }



    public static Result RenderViewCurrentVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewCurrentVisitors.render("View Current Visitors"));
    }


    public static Result RenderEditExitedVisitor(Long id){
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else {
            System.out.println("the aid is " + session("aid"));

            MVisitors mVisitors = MVisitors.findVisitors.byId(id);
            return ok(views.html.Visitors.EditExitedVisitor.render("Edit Exited Visitor", mVisitors, MEmployees.Employees()));
        }
    }





    public static Result EditVisitors(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String aid=requestform.get("aid");
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String Country = requestform.get("Country");
        String Id = requestform.get("IdNumber");
        String phone=requestform.get("PhoneNumber");
        String HostAid = requestform.get("HostAid");
        String Company = requestform.get("Company");
        String Type = requestform.get("VisitType");
        String Gender = requestform.get("Gender");
        String Languages = requestform.get("Language");




        System.out.println("FullName: "+FullName);
        System.out.println("Email: "+Email);
        System.out.println("Country: "+Country);
        System.out.println("IdNumber: "+Id);
        System.out.println("HostAid: "+HostAid);
        System.out.println("Company: "+Company);
        System.out.println("VisitType: "+Type);
        System.out.println("Gender: "+Gender);
        System.out.println("Languages: "+Languages);



      String hostname=MEmployees.findEmployeeByAid(HostAid).FullName;


       MVisitors mVisitors=new MVisitors();
        mVisitors.FullName = FullName;
        mVisitors.Email = Email;
        mVisitors.Country =Country;
        mVisitors.employeeAid=HostAid;
        mVisitors.Company = Company;
        mVisitors.Host = hostname;
        mVisitors.Languages = Languages;
        mVisitors.Type = Type;
        mVisitors.Gender = Gender;
        Long aid2=Long.parseLong(aid);

        mVisitors.update(aid2);


        MVisitorRecords mVisitorRecords1=new MVisitorRecords();
       Long Aid=MVisitorRecords.findVisitorById(Id).aid;
        mVisitorRecords1.FullName = FullName;
        mVisitorRecords1.Email = Email;
        mVisitorRecords1.Country = Country;
        mVisitorRecords1.NationalId = Id;
        mVisitorRecords1.Company = Company;
        mVisitorRecords1.Languages = Languages;
        mVisitorRecords1.Gender = Gender;
        mVisitorRecords1.phoneNumber=phone;
        mVisitorRecords1.update(Aid);

        flash("success", "visitor updated successfully");
        return redirect(routes.CExitedVisitors.RenderExitedVisitors());

    }
    public static Result ActivateVisitor(Long id){
        MVisitors mVisitors = new MVisitors();
        mVisitors.aid=id;
        mVisitors.isActive="1";
        mVisitors.update();

        return redirect(routes.CVisitors.RenderViewVisitors());
    }
    public static Result DeActivateVisitor(Long id){
        MVisitors mVisitors = new MVisitors();
        mVisitors.aid=id;
        mVisitors.isActive="0";
        mVisitors.update();

        return redirect(routes.CVisitors.RenderViewVisitors());
    }

    public static Result VisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "aid";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "aid";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MVisitors> areaPage = MVisitors.findVisitors.where(


                Expr.or(
                        Expr.or(
                                Expr.or(Expr.ilike("FullName", "%" + filter + "%"), Expr.ilike("Date", "%" + filter + "%")),
                                Expr.or(Expr.ilike("Host", "%" + filter + "%"), Expr.ilike("Company", "%" + filter + "%"))
                        ),
                        Expr.or(Expr.ilike("TimeIn", "%" + filter + "%"), Expr.ilike("phoneNumber", "%" + filter + "%"))
                )



                )
                        .orderBy(sortBy + " " + order + ", aid " + order)
                        .findPagingList(pageSize).setFetchAhead(false)
                        .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("TimeOut",cc.TimeOut);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("phoneNumber",cc.phoneNumber);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            //row.put("Image",cc.Image);
            anc.add(row);
        }
        return ok(result);
    }


    public static Result ReccuringVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitorRecords.findVisitorsRecords.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "aid";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "aid";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "NoOfTimes";
                break;
        }

        Page<MVisitorRecords> areaPage = MVisitorRecords.findVisitorsRecords.where(


                Expr.or(
                        Expr.or(
                                Expr.or(Expr.ilike("FullName", "%" + filter + "%"), Expr.ilike("NoOfTimes", "%" + filter + "%")),
                                Expr.or(Expr.ilike("NationalId", "%" + filter + "%"), Expr.ilike("Company", "%" + filter + "%"))
                        ),
                        Expr.or(Expr.ilike("Country", "%" + filter + "%"), Expr.ilike("phoneNumber", "%" + filter + "%"))
                )




        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitorRecords cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("Country", cc.Country);
            row.put("Language", cc.Languages);
            row.put("phoneNumber",cc.phoneNumber);
            row.put("Gender",cc.Gender);
            row.put("Image",cc.Image);


            row.put("NoOfTimes",cc.NoOfTimes);
            anc.add(row);
        }
        return ok(result);
    }






    public static Result CurrentVisitorsList(){
        Map<String, String[]> params = request().queryString();
        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

        String sortBy = "aid";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "aid";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }
        /**
         * Get page to show from database
         * It is important to set setFetchAhead to false, since it doesn't benefit a stateless application at all.
         */
        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("isActive",  "1" ),

                        Expr.or(
                                Expr.or(
                                        Expr.or(Expr.ilike("FullName", "%" + filter + "%"), Expr.ilike("Date", "%" + filter + "%")),
                                        Expr.or(Expr.ilike("Host", "%" + filter + "%"), Expr.ilike("Company", "%" + filter + "%"))
                                ),
                                Expr.or(Expr.ilike("TimeIn", "%" + filter + "%"), Expr.ilike("cardNumber", "%" + filter + "%"))
                        )
                )

        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("phoneNumber", cc.phoneNumber);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            row.put("cardNumber",cc.cardNumber);
            //row.put("Image",cc.Image);
            anc.add(row);
        }
        return ok(result);
    }

    public static Result ExitedVisitors() throws ParseException /*throws ParseException*/ {
        Map<String, String[]> params = request().queryString();
        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;



        String sortBy = "aid";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "aid";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "Date";
                break;
        }

        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("isActive",  "0" ),

                        Expr.or(
                        Expr.or(
                                Expr.or(Expr.ilike("FullName", "%" + filter + "%"), Expr.ilike("Date", "%" + filter + "%")),
                                Expr.or(Expr.ilike("Host", "%" + filter + "%"), Expr.ilike("Company", "%" + filter + "%"))
                        ),
                                Expr.or(Expr.ilike("TimeIn", "%" + filter + "%"), Expr.ilike("TimeOut", "%" + filter + "%"))
                        )
                )

        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();
            row.put("FullName", cc.FullName);
            row.put("NationalId", cc.NationalId);
            row.put("Company", cc.Company);
            row.put("aid", cc.aid);
            row.put("TimeOut",cc.TimeOut);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);

           /*Date  date= new SimpleDateFormat("dd/MM/yyyy").parse(cc.Date);
           Date date2=null;*/
         // row.put("Date",date);

            //Date convertedD;
            row.put("phoneNumber",cc.phoneNumber);
            row.put("cardNumber",cc.cardNumber);
            anc.add(row);
        }
        return ok(result);
    }

    public static Result ExpectedVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MExpectedVisitors.findExpectedVisitorsRecords.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "aid";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "aid";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MExpectedVisitors> areaPage = MExpectedVisitors.findExpectedVisitorsRecords.where(
                Expr.and(Expr.eq("isActive",  "1" ),



                        Expr.or(
                                Expr.or(
                                        Expr.or(Expr.ilike("FullName", "%" + filter + "%"), Expr.ilike("Date", "%" + filter + "%")),
                                        Expr.or(Expr.ilike("HostName", "%" + filter + "%"), Expr.ilike("Company", "%" + filter + "%"))
                                ),
                                Expr.or(Expr.ilike("Email", "%" + filter + "%"), Expr.ilike("phoneNumber", "%" + filter + "%"))
                        )



                )
        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MExpectedVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("Date",cc.Date);
            row.put("visitType", cc.visitType);
            row.put("Country", cc.Country);
            row.put("visitType", cc.visitType);
            row.put("HostName", cc.HostName);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            row.put("phoneNumber",cc.phoneNumber);
            row.put("isActive",cc.isActive);

            anc.add(row);
        }
        return ok(result);
    }



}








