package controllers;
import models.MVisitorRecords;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Visitors.*;

import java.util.Map;
public class CExitedVisitors extends Controller {
    public static Result RenderExitedVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ExitedVisitors.render("Visitors"));
    }


    public static Result ReceiveExitedVisitors(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String Country = requestform.get("Country");
        String Id = requestform.get("Id");
        String Host = requestform.get("Host");
        String Address = requestform.get("Address");
        String Company = requestform.get("Company");
        String Type = requestform.get("VisitType");
        String Gender = requestform.get("Gender");
        String Languages = requestform.get("Languages");
        String TimeOut = requestform.get("TimeOut");
        String Date = requestform.get("Date");
        String Image = requestform.get("Image");

        System.out.println("FullName: "+FullName);
        System.out.println("Email: "+Email);
        System.out.println("Country: "+Country);
        System.out.println("Id: "+Id);
        System.out.println("Host: "+Host);
        System.out.println("Address: "+Address);
        System.out.println("Company: "+Company);
        System.out.println("VisitType: "+Type);
        System.out.println("Gender: "+Gender);
        System.out.println("Languages: "+Languages);
        System.out.println("TimeOut: "+TimeOut);
        System.out.println("Date: "+Date);
        System.out.println("Image: "+Image);
        return redirect(routes.CExitedVisitors.RenderExitedVisitors());
    }




    public static Result ExitedVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitorRecords.findVisitorsRecords.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        /**
         * Get sorting order and column
         */
        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "Id";
                break;
        }
        /**
         * Get page to show from database
         * It is important to set setFetchAhead to false, since it doesn't benefit a stateless application at all.
         */
        Page<MVisitorRecords> areaPage = MVisitorRecords.findVisitorsRecords.where(
                Expr.or(
                        Expr.ilike("FullName", "%" + filter + "%"),
                        Expr.or(
                                Expr.ilike("aid", "%" + filter + "%"),
                                Expr.ilike("Id", "%" + filter + "%")
                        )
                )

        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitorRecords cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.RoomName);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("Id", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Address", cc.Address);
            row.put("Company", cc.Company);
            //row.put("TimeOut",cc.TimeOut);
            row.put("Country", cc.Country);
           // row.put("Type", cc.Type);
           // row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
           // row.put("Date",cc.Date);
            row.put("Image",cc.Image);
            //row.put("cardNumber",cc.cardNumber);
            anc.add(row);
        }
        return ok(result);
    }
}
