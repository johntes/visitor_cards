package controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple Java program to read CSV file in Java. In this program we will read
 * list of books stored in CSV file as comma separated values.
 *
 * @author WINDOWS 8
 *
 */
public class CSVVisitorReaderInJava  {



    public static List<Visitor> readBooksFromCSV(File file) throws IOException {
        List<Visitor> visitors = new ArrayList<>();
        //Path pathToFile = Paths.get(fileName);

        // create an instance of BufferedReader
        // using try with resource, Java 7 feature to close resources

        final BufferedReader br = new BufferedReader(new FileReader(file));


            // read the first line from the text file
            String line = br.readLine();

            // loop until all lines are read
        int iteration=0;
            while (line != null) {

                // use string.split to load a string array with the values from
                // each line of
                // the file, using a comma as the delimiter
//ignoring the first line
                if(iteration == 0) {
                    iteration++;
                    continue;
                }

                String[] attributes = line.split(",");

                Visitor visitor = createVisitor(attributes);

                // adding book into ArrayList
                visitors.add(visitor);

                // read next line before looping
                // if end of file reached, line would be null
                line = br.readLine();
            }




        return visitors;
    }

    public static Visitor createVisitor(String[] metadata) {
        String Organization = "";
        String  FullNames ="";
        String Designation = "";
        String Email ="";
        String MobileNumber = "";


        System.out.println("the length is:"+metadata.length);
        if(metadata.length<1)
        {
         return null;
        }
       else if(metadata.length<2)
        {
            Organization = metadata[0];
        }
       else if(metadata.length<3)
        {
            Organization = metadata[0];
            FullNames = metadata[1];
        }
       else if(metadata.length<4)
        {
            Organization = metadata[0];
            FullNames = metadata[1];
            Designation = metadata[2];
        }
       else if(metadata.length<5)
        {
            Organization = metadata[0];
            FullNames = metadata[1];
            Designation = metadata[2];
            Email = metadata[3];
        }
       else if(metadata.length<6)
        {
            Organization = metadata[0];
            FullNames = metadata[1];
            Designation = metadata[2];
            Email = metadata[3];
            MobileNumber = metadata[4];
        }






        // create and return book of this metadata
        return new Visitor(Organization,FullNames,Designation, Email,MobileNumber);
    }

}

class Visitor {

     private String  FullNames;
     private String Email;
    private String MobileNumber;
    private String Organization;
    private String Designation;



    public Visitor(String Organization,String FullNames,String Designation,String Email,String MobileNumber) {
        this.FullNames=FullNames;
        this.Email=Email;
        this.MobileNumber=MobileNumber;
        this.Organization=Organization;
        this.Designation=Designation;
    }

    public String getFullNames() {
        return FullNames;
    }

    public void setFullNames(String fullNames) {
        FullNames = fullNames;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getOrganization() {
        return Organization;
    }

    public void setOrganization(String organization) {
        Organization = organization;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    @Override
    public String toString() {
        return "Book [name=" + FullNames + ", index=" + Email
                + "]";
    }

}