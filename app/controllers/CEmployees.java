package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.MEmployees;
import models.MEventVisitor;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Employees.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static controllers.CLogin.setSessionsEmployee;

public class CEmployees extends Controller {




    public static Result RenderAddEmployee() {

        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(AddEmployees.render("Add Employees"));
    }


   /* public static Result RenderSetPassword() {

       *//* MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));*//*

        return ok(setPassword.render("set Password"));
    }*/




    public static Result RenderEditEmployee(Long id){
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else {
            System.out.println("the aid is " + session("aid"));

            MEmployees mEmployees = MEmployees.findEmployees.byId(id);
            return ok(EditEmployees.render("Edit Employee", mEmployees));
        }
        }



    public static Result RenderViewEmployee() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(ViewEmployees.render("View Employees"));
    }


    public static Result RenderImportEmployees() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else {
            System.out.println("the aid is " + session("aid"));
            return ok(importEmployeesCsv.render("import employees"));
        }
    }



    public static Result EditEmployee(Long id){
        MEmployees mEmployees =new MEmployees();
        mEmployees.aid=id;
        mEmployees.FullName=Form.form().bindFromRequest().get("FullName");
        mEmployees.Email=Form.form().bindFromRequest().get("Email");
        mEmployees.IdNumber=Form.form().bindFromRequest().get("IdNumber");
        mEmployees.Department=Form.form().bindFromRequest().get("Department");
        mEmployees.EmploymentNumber=Form.form().bindFromRequest().get("EmploymentNumber");
        Ebean.update(mEmployees);
        return redirect(routes.CEmployees.RenderViewEmployee());
    }



    public static Result setPassword(){
              MEmployees mEmployees =new MEmployees();
        String encryptedPassword=Encryption.encryptString(Form.form().bindFromRequest().get("password"));


              System.out.println("encrypted password");
              mEmployees.password=encryptedPassword;
              mEmployees.password_set="1";
              String aid=Form.form().bindFromRequest().get("aid");
              System.out.println("the aid is:"+aid);
              Long Aid=Long.parseLong(aid);
              mEmployees.update(Aid);

              String email=MEmployees.findEmployeeByAid(aid).Email;
              String idNumber=MEmployees.findEmployeeByAid(aid).IdNumber;

              CLogin.setSessionsEmployee(MEmployees.authenticateEmployee(email, idNumber));
              flash("success", "Your password was set successfully. You can now login using your email and the set password");

              System.out.println("The employee session has been set");
              return redirect(routes.CDashboard.EmployeedashboardRender());
    }

    public static Result activateEmployee(Long id){
        MEmployees mEmployees = new MEmployees();
        mEmployees.aid=id;
        mEmployees.isActive="1";
        mEmployees.update();

        return redirect(routes.CEmployees.RenderViewEmployee());
    }
    public static Result deactivateEmployee(Long id){
        MEmployees mEmployees = new MEmployees();
        mEmployees.aid=id;
        mEmployees.isActive="0";
        mEmployees.update();

        return redirect(routes.CEmployees.RenderViewEmployee());
    }

    public static Result ReceiveEmployees() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String IdNumber = requestform.get("IdNumber");
        String Department = requestform.get("Department");
        String EmploymentNumber = requestform.get("EmploymentNumber");

        System.out.println("FullName: "+FullName);
        System.out.println("Email: "+Email);
        System.out.println("IdNumber: "+IdNumber);
        System.out.println("Department: "+Department);
        System.out.println("EmploymentNumber: "+EmploymentNumber);

        return ok();
    }

        public static Result AddEmployees () {
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String IdNumber = requestform.get("IdNumber");
        String Department = requestform.get("Department");
        String EmploymentNumber = requestform.get("EmploymentNumber");
        String PhoneNumber = requestform.get("PhoneNumber");

      int len=PhoneNumber.length();

      if((MEmployees.findEmployee(IdNumber)!=null) && (MEmployees.findEmployeeNumber(EmploymentNumber)!=null)){

          flash("typeerror", "employee with such Id or employment number exists!!!!");
          return redirect(routes.CEmployees.RenderAddEmployee());
      }

      else if( !(len>=10 && len<=15)){

          flash("typeerror", "phone digits should be between 10 and 15!!!!");
          return redirect(routes.CEmployees.RenderAddEmployee());

      }



       else if((MEmployees.findEmployee(IdNumber)==null) && (MEmployees.findEmployeeNumber(EmploymentNumber)==null) && (len>=10 && len<=15) )

        {
            System.out.println("Matched");
            MEmployees mEmployees = new MEmployees();
            mEmployees.FullName = FullName;
            mEmployees.Email = Email;
            mEmployees.IdNumber = IdNumber;
            mEmployees.Department = Department;
            mEmployees.EmploymentNumber = EmploymentNumber;
            mEmployees.PhoneNumber = PhoneNumber;
            mEmployees.isActive = "1";
            mEmployees.save();
            return redirect(routes.CEmployees.RenderViewEmployee());
        }
        else
            flash("typeerror", "employee with such Id or employment number exists!!!!");
            return redirect(routes.CEmployees.RenderAddEmployee());

            }


    public static Result addEmployeesFromCsv() throws IOException {

        Http.MultipartFormData body = request().body().asMultipartFormData();
        if(body == null)
        {
            return badRequest("Invalid request, required is POST with enctype=multipart/form-data.");
        }

        Http.MultipartFormData.FilePart filePart = body.getFile("employeeFile");
        if(filePart == null)
        {
            return badRequest("Invalid request, no file has been sent.");
        }


        File file = filePart.getFile();


        List<Employee> employee =CSVEmployeeReaderInJava.readBooksFromCSV(file);

        System.out.println("......................................................................");
        // let's print all the person read from CSV file
        int iteration=0;
        int len= MEmployees.findAll().size();
        System.out.println("the lenght before insert is :"+len);
        int len2=0;
        for (Employee v : employee) {
            if(v.getEmail()==null)
            {

                continue;
            }
            if(iteration == 0) {

                String  FullNames=v.getFullNames();
                String Email=v.getEmail();
                String IdNumber=v.getIdNumber();
                String EmploymentNumber=v.getEmploymentNumber();
               // String MobileNumber=v.getMobileNumber();
                String Department=v.getDepartment();

                iteration++;
                continue;
            }

            System.out.print(iteration+"\t");
            String  FullNames=v.getFullNames();
            String Email=v.getEmail();
            String IdNumber=v.getIdNumber();
            String EmploymentNumber=v.getEmploymentNumber();
            //String MobileNumber=v.getMobileNumber();
            String Department=v.getDepartment();
            System.out.println("The department is "+Department);
            System.out.println("The employment number is "+EmploymentNumber);
            System.out.println("");
            //String NotAdded


            if (MEmployees.findEmployeeNumber(EmploymentNumber) == null && MEmployees.authenticateEmployee(Email,IdNumber)==null) {
                MEmployees mEmployees = new MEmployees();
                mEmployees.FullName=FullNames;
                mEmployees.Email=Email;
                mEmployees.IdNumber=IdNumber;
                mEmployees.EmploymentNumber=EmploymentNumber;
                mEmployees.Department=Department;
               // mEmployees.PhoneNumber=MobileNumber;
                mEmployees.isActive="1";
                mEmployees.save();
                len2 = MEmployees.findAll().size();
                System.out.println("the lenght afetr insert is :"+len2);

                iteration++;
            }
        }
        if(len2>len){
            flash("success", "successfully added unavailable employees from file");
        }
        else {
            flash("typeerror", "failed to add employees,the csv is null or all the employees exist");
        }

        System.out.print(iteration+" this is the iterations");

        return redirect(routes.CEmployees.RenderViewEmployee());


    }



        public static Result EmployeeList () {
            Map<String, String[]> params = request().queryString();

            Integer iTotalRecords = MEmployees.findEmployees.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

            /**
             * Get sorting order and column
             */
            String sortBy = "Email";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "Email";
                    break;
                case 1:
                    sortBy = "aid";
                    break;
                case 2:
                    sortBy = "EmploymentNumber";
                    break;
            }

            /**
             * Get page to show from database
             * It is important to set setFetchAhead to false, since it doesn't benefit a stateless application at all.
             */
            Page<MEmployees> areaPage = MEmployees.findEmployees.where(
                    Expr.or(
                            Expr.ilike("Email", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("EmploymentNumber", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();

            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MEmployees cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                //    System.out.println("in data table fetch: " + cc.UserName);

                row.put("aid", cc.aid);
                row.put("Email", cc.Email);
                row.put("FullName", cc.FullName);
                row.put("isActive", cc.isActive);
                row.put("IdNumber", cc.IdNumber);
                row.put("Department", cc.Department);
                row.put("EmploymentNumber", cc.EmploymentNumber);
                row.put("isActive", cc.isActive);
                if(cc.isActive.equals("1"))
                {
                    row.put("active","yes");
                }
                else {
                    row.put("active","no");
                }
                anc.add(row);
            }

            return ok(result);

        }

    }
