package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.MEvents;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Users.*;
import views.html.Events.*;

import java.security.GeneralSecurityException;
import java.util.Map;

import static controllers.CDashboard.isEventLoggedIn;
import static controllers.EncryptionTest.encrypt;

public class CEvents extends Controller{
  //static   String encryptedPasswor="";


    /*public static MUsers isLoggedIn() {
        String aid = session("aid");
        try {
            MUsers user = MUsers.findUsers.byId(Long.parseLong(aid));
            return user;
        }catch(Exception e){
            return null;
        }

    }*/



    public static Result RenderAddEvent() {
        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(AddEvents.render("Add User"));
    }


    public static Result RenderViewEvents() {

        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));


        return ok(ViewEvents.render("View Events"));
    }

    public static Result RenderViewOnComingEvents() {

        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));


        return ok(ViewOnComingEvents.render("View On Coming Events"));
    }

    public static Result RenderViewOnGoingEvents() {

        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));


        return ok(ViewOngoingEvents.render("View On going Events"));
    }

    public static Result RenderViewOnCompletedEvents() {

        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));


        return ok(ViewCompletedEvents.render("View Completed Events"));
    }


    /*public static Result ActivateUser(Long id){
        MUsers mUsers = new MUsers();
        mUsers.aid=id;
        mUsers.isActive="1";
        mUsers.update();

        return redirect(routes.CUsers.RenderViewUsers());
    }*/
    /*public static Result DeActivateUser(Long id){
        MUsers mUsers = new MUsers();
        mUsers.aid=id;
        mUsers.isActive="0";
        mUsers.update();

        return redirect(routes.CUsers.RenderViewUsers());
    }*/


    public static Result AddEvent() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String eventName = requestform.get("eventName");
        String eventVenue = requestform.get("eventVenue");
        String eventDate = requestform.get("eventDate");

        System.out.println("eventName: "+eventName);
        System.out.println("eventVenue: "+eventVenue);
        System.out.println("eventDate: "+eventDate);

        if(MEvents.findEventByEventName(eventName)!=null)
        {
            flash("typeerror", " That event already exist ");
            return redirect(routes.CEvents.RenderViewEvents());
        }

        else{
            MEvents mEvents=new MEvents();
            mEvents.eventName=eventName;
            mEvents.eventDate=eventDate;
            mEvents.eventVenue=eventVenue;
            mEvents.eventStatus="oncoming";
            mEvents.save();
            flash("typesuccess", " Event Successfully Added ");
            return redirect(routes.CEvents.RenderViewEvents());
        }

    }



        public static Result eventList () {
            Map<String, String[]> params = request().queryString();

            Integer iTotalRecords = MEvents.findEvents.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

            /**
             * Get sorting order and column
             */
            String sortBy = "Email";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "eventDate";
                    break;
                case 1:
                    sortBy = "aid";
                    break;
                case 2:
                    sortBy = "eventName";
                    break;
            }

            Page<MEvents> areaPage = MEvents.findEvents.where(
                    Expr.or(
                            Expr.ilike("eventDate", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("eventName", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();

            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MEvents cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                //    System.out.println("in data table fetch: " + cc.UserName);

                row.put("aid", cc.aid);
                row.put("eventName", cc.eventName);
                row.put("eventDate", cc.eventDate);
                row.put("eventVenue", cc.eventVenue);
                if(cc.eventStatus.equals("oncoming")){
                    row.put("eventStatus", "upcoming");
                }
                else
                {
                    row.put("eventStatus", cc.eventStatus);
                }
                row.put("eventCheckOutTime", cc.eventCheckOutTime);

                anc.add(row);
            }

            return ok(result);

        }


    public static Result OnComingeventList () {
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MEvents.findEvents.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

        /**
         * Get sorting order and column
         */
        String sortBy = "Email";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "eventDate";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "eventName";
                break;
        }

        Page<MEvents> areaPage = MEvents.findEvents.where(
                Expr.and(Expr.eq("eventStatus",  "oncoming" ),
                Expr.or(
                        Expr.ilike("eventDate", "%" + filter + "%"),
                        Expr.or(
                                Expr.ilike("aid", "%" + filter + "%"),
                                Expr.ilike("eventName", "%" + filter + "%")
                        )
                )
        )
        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);

        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");

        for (MEvents cc : areaPage.getList()) {
            ObjectNode row = Json.newObject();
            //    System.out.println("in data table fetch: " + cc.UserName);

            row.put("aid", cc.aid);
            row.put("eventName", cc.eventName);
            row.put("eventDate", cc.eventDate);
            row.put("eventVenue", cc.eventVenue);



            anc.add(row);
        }

        return ok(result);

    }

    public static Result OnGoingeventList () {
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MEvents.findEvents.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

        /**
         * Get sorting order and column
         */
        String sortBy = "Email";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "eventDate";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "eventName";
                break;
        }

        Page<MEvents> areaPage = MEvents.findEvents.where(
                Expr.and(Expr.eq("eventStatus",  "active" ),
                        Expr.or(
                                Expr.ilike("eventDate", "%" + filter + "%"),
                                Expr.or(
                                        Expr.ilike("aid", "%" + filter + "%"),
                                        Expr.ilike("eventName", "%" + filter + "%")
                                )
                        )
                )
        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);

        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");

        for (MEvents cc : areaPage.getList()) {
            ObjectNode row = Json.newObject();
            //    System.out.println("in data table fetch: " + cc.UserName);

            row.put("aid", cc.aid);
            row.put("eventName", cc.eventName);
            row.put("eventDate", cc.eventDate);
            row.put("eventVenue", cc.eventVenue);



            anc.add(row);
        }

        return ok(result);

    }


    public static Result OnCompletedeventList () {
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MEvents.findEvents.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

        /**
         * Get sorting order and column
         */
        String sortBy = "Email";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "eventDate";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "eventName";
                break;
        }

        Page<MEvents> areaPage = MEvents.findEvents.where(
                Expr.and(Expr.eq("eventStatus",  "ended" ),
                        Expr.or(
                                Expr.ilike("eventDate", "%" + filter + "%"),
                                Expr.or(
                                        Expr.ilike("aid", "%" + filter + "%"),
                                        Expr.ilike("eventName", "%" + filter + "%")
                                )
                        )
                )
        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);

        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");

        for (MEvents cc : areaPage.getList()) {
            ObjectNode row = Json.newObject();
            //    System.out.println("in data table fetch: " + cc.UserName);

            row.put("aid", cc.aid);
            row.put("eventName", cc.eventName);
            row.put("eventDate", cc.eventDate);
            row.put("eventVenue", cc.eventVenue);



            anc.add(row);
        }

        return ok(result);

    }


}
