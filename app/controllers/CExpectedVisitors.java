package controllers;


import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.*;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.ExpectedVisitor.*;
import views.html.ExpectedVisitor.AddExpectedVisitor;
import views.html.Visitors.DisplayImage;
import views.html.Visitors.ViewCurrentVisitors;
import views.html.Visitors.ViewRecurringVisitors;

import java.util.Map;

import static controllers.CDashboard.isLoggedIn;

public class CExpectedVisitors extends Controller {

    public static Result RenderAddExpectedVisitors() {

        return ok(AddExpectedVisitor.render("Expected Visitors"));
    }

    public static Result RenderViewReccurrentVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewRecurringVisitors.render("Recurring Visitors"));
    }


    public static Result RenderViewYourExpectedVisitorsDetails(Long id){
        MExpectedVisitors mExpectedVisitors= MExpectedVisitors.findExpectedVisitorsRecords.byId(id);
        return ok(views.html.ExpectedVisitor.DisplayFullExpectedVisitorsDetails.render("mvisitor records",mExpectedVisitors));

    }





    public static Result RenderViewYourExpectedVisitors() {
        MEmployees user = CDashboard.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewYourExpectedVisitors.render("View  your Expected Visitors"));
    }

    public static Result RenderViewTotalVisitors() {
        MEmployees user = CDashboard.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewYourVisitors.render("View  your total Visitors"));
    }


   public static String vip="no";

    public static Result AddExpectedVisitors(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String Country = requestform.get("Country");
        String Id = requestform.get("IdNumber");
        String visitType = requestform.get("visitType");
        String Company = requestform.get("Company");
        String Gender = requestform.get("Gender");
        String Languages = requestform.get("Language");
        String Date = requestform.get("Date");
        String PhoneNumber = requestform.get("PhoneNumber");
        String time=requestform.get("time");



        if(requestform.get("vip")==null)
        {
            System.out.println("THE VIP IS "+requestform.get("vip"));
            System.out.println("the vip button was not checked");
        }
        else{
            System.out.println("the vip button was checked");
            vip="yes";
            System.out.println("THE VIP IS "+requestform.get("vip"));
        }



        int len=PhoneNumber.length();
         if( !(len>=10 && len<=15)){

            flash("typeerror", "phone digits should be between 10 and 15!!!!");
            return redirect(routes.CExpectedVisitors.RenderAddExpectedVisitors());

        }

      else  if (isLoggedIn() == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else if(MExpectedVisitors.findActiveVisitorById(Id)!=null){
            flash("typeerror", "That Visitor is already expected");
            return redirect(routes.CExpectedVisitors.RenderAddExpectedVisitors());
        }



        else {
            String host=isLoggedIn().FullName;
            String employeeId=isLoggedIn().IdNumber;
            MExpectedVisitors mExpectedVisitors = new MExpectedVisitors();
            mExpectedVisitors.Company = Company;
            mExpectedVisitors.Country = Country;
            mExpectedVisitors.Date = Date;
            mExpectedVisitors.Email = Email;
            mExpectedVisitors.Languages = Languages;
            mExpectedVisitors.NationalId = Id;
            mExpectedVisitors.Gender = Gender;
            mExpectedVisitors.phoneNumber = PhoneNumber;
            mExpectedVisitors.visitType = visitType;
            mExpectedVisitors.FullName = FullName;
            mExpectedVisitors.HostName=host;
            mExpectedVisitors.employeeId=employeeId;
            mExpectedVisitors.time=time;
           // mExpectedVisitors.hasVisited="0";
            mExpectedVisitors.isActive="1";
            mExpectedVisitors.vip=vip;
            mExpectedVisitors.save();
            vip="no";
             flash("success", " expected visitor added successfully");
        }

        return redirect(routes.CExpectedVisitors.RenderViewYourExpectedVisitors());
    }
    public static Result ActivateExpectedVisitor(Long id){
        MExpectedVisitors mExpectedVisitors = new MExpectedVisitors();
        mExpectedVisitors.aid=id;
        mExpectedVisitors.isActive="1";
        mExpectedVisitors.update();

        return redirect(routes.CExpectedVisitors.RenderViewYourExpectedVisitors());
    }

    public static Result DeactivateExpectedVisitor(Long id){
        MExpectedVisitors mExpectedVisitors = new MExpectedVisitors();
        mExpectedVisitors.aid=id;
        mExpectedVisitors.isActive="0";
        mExpectedVisitors.update();

        return redirect(routes.CExpectedVisitors.RenderViewYourExpectedVisitors());
    }



    public static Result DeleteVisitor(Long id){
        MVisitors mVisitors = new MVisitors();
        long Aid= MVisitors.findVisitorById(id.toString()).aid;
        mVisitors.isDeleted="1";
        mVisitors.update(Aid);

        //getting card number and free the cards too
        String cardnumber= MVisitors.findVisitorById(id.toString()).cardNumber;
        MCards mCards=new MCards();
        long Aid2= MCards.exitCard(cardnumber).aid;
        mCards.isActive="0";
        mCards.update(Aid2);

        return redirect(routes.CVisitors.RenderViewVisitors());
    }

    public static Result YourExpectedVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MExpectedVisitors.findExpectedVisitorsRecords.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "aid";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "aid";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MExpectedVisitors> areaPage = MExpectedVisitors.findExpectedVisitorsRecords.where(
                Expr.and(Expr.eq("employeeId",  isLoggedIn().IdNumber ),
                        Expr.and(Expr.eq("isActive",  "1" ),
                                Expr.or(
                                        Expr.or(
                                                Expr.or(Expr.ilike("FullName", "%" + filter + "%"), Expr.ilike("Date", "%" + filter + "%")),
                                                Expr.or(Expr.ilike("HostName", "%" + filter + "%"), Expr.ilike("Company", "%" + filter + "%"))
                                        ),
                                        Expr.or(Expr.ilike("visitType", "%" + filter + "%"), Expr.ilike("phoneNumber", "%" + filter + "%"))
                                )
                )
                )
        )      .orderBy(sortBy + " " + order + ", aid " + order)
                        .findPagingList(pageSize).setFetchAhead(false)
                        .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MExpectedVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("Date",cc.Date);
            row.put("visitType", cc.visitType);
            row.put("Country", cc.Country);
            row.put("visitType", cc.visitType);
            row.put("HostName", cc.HostName);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            row.put("phoneNumber",cc.phoneNumber);
            row.put("isActive",cc.isActive);

            anc.add(row);
        }
        return ok(result);
    }

/*
    public static Result YourVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("NationalId",  isLoggedIn().IdNumber ),
                Expr.or(
                        Expr.ilike("FullName", "%" + filter + "%"),
                        Expr.or(
                                Expr.ilike("aid", "%" + filter + "%"),
                                Expr.ilike("NationalId", "%" + filter + "%")
                        )
                )

        ))
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("TimeOut",cc.TimeOut);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            anc.add(row);
        }
        return ok(result);
    }*/

    public static Result YourVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "aid";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "aid";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("employeeAid",  isLoggedIn().aid ),
                        Expr.or(
                                Expr.ilike("FullName", "%" + filter + "%"),
                                Expr.or(
                                        Expr.ilike("aid", "%" + filter + "%"),
                                        Expr.ilike("NationalId", "%" + filter + "%")
                                )
                        )

                ))
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("TimeOut",cc.TimeOut);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            anc.add(row);
        }
        return ok(result);
    }








}








