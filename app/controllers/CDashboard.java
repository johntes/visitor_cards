package controllers;

import models.*;
import play.*;
import play.mvc.*;
import views.html.Cards.ViewCards;
import views.html.Dashboard.*;

import models.MUsers;
import models.MVisitorRecords;
import models.MVisitors;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Visitors.*;

import java.util.Date;
import java.util.Map;

import views.html.Employees.*;
import views.html.Events.*;

public class CDashboard extends Controller {

    public static  Result renderChart(){
        return ok(chat.render("render charts"));
    }



    public static Result dashboardRender() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        } else {
            int numberOfAdmins = MUsers.findUsers.findRowCount();
            int numberOfEmployees = MEmployees.findEmployees.findRowCount();
            int numberOfTotalVisitors = MVisitors.findVisitors.findRowCount();
            int numberOfCurrentVisitors = MVisitors.findActiveVisitors().size();
            int numberOfExitedVisitors = MVisitors.findDiActivetedVisitors().size();
            int numberOfExpectedVisitors = MExpectedVisitors.findActiveExpctedVisitorList().size();

            String config="";
            if(MConfig_table.findConfig("Using QR Codes")!=null){
                config="Using QR Codes";
            }

            else if(MConfig_table.findConfig("Using Cards")!=null){
                config="Using Cards";
            }

            else {
                config="Not Configured" ;
            }

            return ok(dashboard.render("DashBoard", numberOfAdmins, numberOfEmployees, numberOfCurrentVisitors, numberOfTotalVisitors, numberOfExitedVisitors, numberOfExpectedVisitors));
        }
    }

    public static Result EventdashboardRender() {
        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        } else {
            System.out.println("the aid is " + session("aid"));
            int numberOfTotalEvents = MEvents.findEvents.findRowCount();
            int numberOfOncomingEvents = MEvents.findOnComingEvents().size();
            int numberOfOngoingEvents = MEvents.findActiveEvents().size();
            int numberOfCompletedEvents = MEvents.findEndedEvents().size();
            return ok(dashboardEvent.render("Event DashBoard", numberOfTotalEvents, numberOfOncomingEvents, numberOfCompletedEvents, numberOfOngoingEvents));
        }
    }




    public static MEmployees isLoggedIn() {
        String aid = session("aid");
        try {
            MEmployees user = MEmployees.findEmployees.byId(Long.parseLong(aid));
            return user;
        }catch(Exception e){
            return null;
        }
    }

    public static MUsers isEventLoggedIn() {
        String aid = session("aid");
        Long Aid=Long.parseLong(aid);
        try {
            MUsers user = MUsers.findEventWebUserByAid(Aid);
            return user;
        }catch(Exception e){
            return null;
        }
    }




    public static Result EmployeedashboardRender() {

        if (isLoggedIn() == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else {
            MEmployees emp = MEmployees.findEmployees.byId(Long.parseLong(session("aid")));
            String name = isLoggedIn().FullName;
            int numberOfExpectedVisitors = MExpectedVisitors.findYourVistorList(isLoggedIn().IdNumber).size();
            return ok(dashboardEmployee.render("DashBoard", name, numberOfExpectedVisitors));
        }
    }



    public static Result return_visitors(){
        return ok(Json.toJson(MVisitors.findAll()));
    }

}
