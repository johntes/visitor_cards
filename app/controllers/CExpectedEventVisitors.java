package controllers;


import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.*;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.ExpectedVisitor.*;
import views.html.Events.*;
import views.html.ExpectedVisitor.AddExpectedVisitor;
import views.html.Visitors.ViewRecurringVisitors;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static controllers.CDashboard.isEventLoggedIn;
import static controllers.CDashboard.isLoggedIn;

public class CExpectedEventVisitors extends Controller {


    public static Result RenderAddExpectedEventVisitors(Long id) {

        System.out.println("the id is:"+id);
        String eventName=MEvents.findEventById(id).eventName;
        Long eventAid=MEvents.findEventById(id).aid;
        System.out.println("the event aid being sent is :"+eventAid);

        String eventAid2=eventAid.toString();
        System.out.println("the event venue  "+eventName);
        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(AddExpectedEventVisitor.render("Expected Event Visitors",eventAid2));
    }


    public static Result RenderImportVisitors() {
        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else {
            DynamicForm requestform = Form.form().bindFromRequest();
            String eventAid = requestform.get("eventAid");

            System.out.println("the event name is :" + eventAid);
            System.out.println("the aid is " + session("aid"));
            return ok(importVisitorsCsv.render("import Event Visitors",eventAid));
        }
    }


    public static Result RenderViewExpectedEventVisitors(Long id) {
        Long eventAid=MEvents.findEventById(id).aid;
        System.out.println("the eventAid is:" +eventAid);
        Long aid=id;
        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewExpectedEventVisitors.render("View  your Expected Visitors",eventAid));
    }

    public static Result RenderViewAttendedEventVisitors(String id) {
        Long aid2=Long.parseLong(id);
        Long eventAid=MEvents.findEventById(aid2).aid;
        System.out.println("the event Name is:" +eventAid);
        //Long aid=id;
        MUsers user = isEventLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewAttendedEventVisitors.render("View  Attended Visitors",eventAid));
    }





    public static Result ReceiveExpectedEventVisitors(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String PhoneNumber = requestform.get("PhoneNumber");
        String Company = requestform.get("Company");
        String Confirmation = requestform.get("Confirmation");
        String Designation = requestform.get("Designation");
        String eventAid = requestform.get("eventAid");
        String timeIn = requestform.get("timeIn");
        System.out.println("the event aid is.....................> "+eventAid);


        int len=PhoneNumber.length();
         if( !(len>=10 && len<=15)){
            flash("typeerror", "phone digits should be between 10 and 15!!!!");
            return redirect(routes.CExpectedEventVisitors.RenderImportVisitors());
        }

      else  if (isEventLoggedIn() == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }

        else if(MEventVisitor.findVisitorByEmailAndEvent(Email,eventAid)!=null){
            flash("typeerror", "That Visitor is already expected");
            return redirect(routes.CExpectedEventVisitors.RenderImportVisitors());
        }
        else {
             MEventVisitor mEventVisitor = new MEventVisitor();
             mEventVisitor.Organisation = Company;
             mEventVisitor.Email = Email;
             mEventVisitor.Designation = Designation;
             mEventVisitor.Confirmation = Confirmation;
             mEventVisitor.PhoneNumber = PhoneNumber;
             mEventVisitor.FullName = FullName;
             mEventVisitor.eventAid=eventAid;
             mEventVisitor.Expected="yes";
             mEventVisitor.Attended="0";
             mEventVisitor.save();
        }
        return redirect(routes.CEvents.RenderViewEvents());
    }


    public static Result addVisitorFromCsv() throws IOException {

        Http.MultipartFormData body = request().body().asMultipartFormData();
        if(body == null)
        {
            return badRequest("Invalid request, required is POST with enctype=multipart/form-data.");
        }

        Http.MultipartFormData.FilePart filePart = body.getFile("cardFile");
        if(filePart == null)
        {
            return badRequest("Invalid request, no file has been sent.");
        }


        File file = filePart.getFile();

        DynamicForm dynamicForm=Form.form().bindFromRequest();
        String eventAid=dynamicForm.get("eventAid");
        System.out.println("eventAid sent is ------------"+eventAid);
        //String eventName=body.getText("eventName");


        List<Visitor> visitors =CSVVisitorReaderInJava.readBooksFromCSV(file);

        System.out.println("......................................................................");
        // let's print all the person read from CSV file
        int iteration=0;
        int len=MEventVisitor.findAll().size();
        int len2=0;
        for (Visitor v : visitors) {
            if(v.getEmail()==null)
            {

                continue;
            }
            if(iteration == 0) {

                String  FullNames = v.getFullNames();
                String Email = v.getEmail();
                String MobileNumber = v.getMobileNumber();
                String Organization =v.getOrganization();
                String Designation = v.getDesignation();

                iteration++;
                continue;
            }

            System.out.print(iteration+"\t");
            String  FullNames = v.getFullNames();
            String Email = v.getEmail();
            String MobileNumber = v.getMobileNumber();
            String Organization =v.getOrganization();
            String Designation = v.getDesignation();

            System.out.println("");


            if (MEventVisitor.findVisitorByEmailAndEvent(Email,eventAid) == null) {
                MEventVisitor mEventVisitor = new MEventVisitor();
                mEventVisitor.FullName=FullNames;
                mEventVisitor.Email=Email;
                mEventVisitor.PhoneNumber=MobileNumber;
                mEventVisitor.Organisation=Organization;
                mEventVisitor.Designation=Designation;
                mEventVisitor.eventAid=eventAid;
                mEventVisitor.Expected="yes";
                mEventVisitor.Attended="0";
                mEventVisitor.save();
                len2 = MEventVisitor.findAll().size();
                iteration++;
            }
        }
        if(len2>len){
            flash("typesuccess", "successfully added unavailable visitor from file");
        }
        else {
            flash("typeerror", "failed to add");
        }

        System.out.print(iteration+" this is the iterations");

        return redirect(routes.CEvents.RenderViewEvents());

    }





    public static Result ActivateExpectedVisitor(Long id){
        MExpectedVisitors mExpectedVisitors = new MExpectedVisitors();
        mExpectedVisitors.aid=id;
        mExpectedVisitors.isActive="1";
        mExpectedVisitors.update();

        return redirect(routes.CExpectedVisitors.RenderViewYourExpectedVisitors());
    }

    public static Result DeactivateExpectedVisitor(Long id){
        MExpectedVisitors mExpectedVisitors = new MExpectedVisitors();
        mExpectedVisitors.aid=id;
        mExpectedVisitors.isActive="0";
        mExpectedVisitors.update();

        return redirect(routes.CExpectedVisitors.RenderViewYourExpectedVisitors());
    }
    public static Result RenderViewDetails(Long id){
        if(id==null)
        {
            System.out.println("an error occured");
        }
        MEventVisitor mEventVisitor= MEventVisitor.findEventVisitors.byId(id);
        return ok(DisplayDetails.render("Attenders details",mEventVisitor));

    }



    public static Result DeleteVisitor(Long id){
        MVisitors mVisitors = new MVisitors();
        long Aid= MVisitors.findVisitorById(id.toString()).aid;
        mVisitors.isDeleted="1";
        mVisitors.update(Aid);

        //getting card number and free the cards too
        String cardnumber= MVisitors.findVisitorById(id.toString()).cardNumber;
        MCards mCards=new MCards();
        long Aid2= MCards.exitCard(cardnumber).aid;
        mCards.isActive="0";
        mCards.update(Aid2);

        return redirect(routes.CVisitors.RenderViewVisitors());
    }

    /*Expr.and(Expr.eq("Expected",  "yes" ),
    ExpectedEventVisitorsList */

   /* String Attended=cc.Attended;

            if(Attended.equals("1"))
    {
        row.put("attended","yes");
    }
            else
    {
        row.put("attended","no");
    }*/

   /* public static Result ExpectedEventVisitorsList(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String eventAid = requestform.get("eventAid");
        Map<String, String[]> params = request().queryString();
        Integer iTotalRecords = MEventVisitor.findEventVisitors.findRowCount();

        //just bytha
        int numberOfRecords=MEventVisitor.findEventVistorList(eventAid).size();
        System.out.println("the number of people is ------->"+numberOfRecords);

        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "Email";
                break;
        }

        Page<MEventVisitor> areaPage = MEventVisitor.findEventVisitors.where(
                //Expr.and(Expr.eq("eventAid", eventAid),
                Expr.and(Expr.eq("Expected",  "yes" ),

                        Expr.or(
                        Expr.ilike("FullName", "%" + filter + "%"),
                        Expr.or(
                                Expr.ilike("aid", "%" + filter + "%"),
                                Expr.ilike("Email", "%" + filter + "%")
                        )
                )
                )
                )
                //)
                        .orderBy(sortBy + " " + order + ", aid " + order)
                        .findPagingList(pageSize).setFetchAhead(false)
                        .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MEventVisitor cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Organisation", cc.Organisation);
            row.put("PhoneNumber",cc.PhoneNumber);
            row.put("Country", cc.Country);
            row.put("Confirmation", cc.Confirmation);
            row.put("Designation", cc.Designation);
            row.put("Gender",cc.Gender);
            String Attended=cc.Attended;

            if(Attended.equals("1"))
            {
                row.put("attended","yes");
            }
            else
            {
                row.put("attended","no");
            }

            anc.add(row);
        }
        return ok(result);
    }
*/

    public static Result ExpectedEventVisitorsList(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String eventAid = requestform.get("eventAid");
        System.out.println("the eventAid listed  is:  "+eventAid);

        Map<String, String[]> params = request().queryString();
        Integer iTotalRecords = MEventVisitor.findEventVisitors.findRowCount();

        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "Email";
                break;
        }

        Page<MEventVisitor> areaPage = MEventVisitor.findEventVisitors.where(
                Expr.and(Expr.eq("Expected",  "yes" ),
                        Expr.and(Expr.eq("eventAid", eventAid),
                                Expr.or(
                                        Expr.ilike("FullName", "%" + filter + "%"),
                                        Expr.or(
                                                Expr.ilike("aid", "%" + filter + "%"),
                                                Expr.ilike("Email", "%" + filter + "%")
                                        )
                                )
                        )
                )
        )        .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MEventVisitor cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("timeIn", cc.timeIn);
            row.put("Email", cc.Email);
            row.put("Organisation", cc.Organisation);
            row.put("PhoneNumber",cc.PhoneNumber);
            row.put("Country", cc.Country);
            row.put("Designation", cc.Designation);
            row.put("Confirmation", cc.Confirmation);
            row.put("Gender",cc.Gender);

            String Attended=cc.Attended;

            if(Attended.equals("1"))
            {
                row.put("attended","yes");
            }
            else
            {
                row.put("attended","no");
            }



            anc.add(row);
        }
        return ok(result);
    }


    public static Result AttendedEventVisitorsList(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String eventAid = requestform.get("eventAid");
        System.out.println("the eventAid listed  is:  "+eventAid);

        Map<String, String[]> params = request().queryString();
        Integer iTotalRecords = MEventVisitor.findEventVisitors.findRowCount();

        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "Email";
                break;
        }

        Page<MEventVisitor> areaPage = MEventVisitor.findEventVisitors.where(
                Expr.and(Expr.eq("Attended",  "1" ),
                        Expr.and(Expr.eq("eventAid", eventAid),
                        Expr.or(
                                Expr.ilike("FullName", "%" + filter + "%"),
                                Expr.or(
                                        Expr.ilike("aid", "%" + filter + "%"),
                                        Expr.ilike("Email", "%" + filter + "%")
                                )
                        )
                )
        )
        )        .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MEventVisitor cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("timeIn", cc.timeIn);
            row.put("Email", cc.Email);
            row.put("Organisation", cc.Organisation);
            row.put("PhoneNumber",cc.PhoneNumber);
            row.put("Country", cc.Country);
            row.put("Designation", cc.Designation);
            row.put("Confirmation", cc.Confirmation);
            row.put("Gender",cc.Gender);



            anc.add(row);
        }
        return ok(result);
    }





    public static Result YourVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("NationalId",  isLoggedIn().IdNumber ),
                        Expr.or(
                                Expr.ilike("FullName", "%" + filter + "%"),
                                Expr.or(
                                        Expr.ilike("aid", "%" + filter + "%"),
                                        Expr.ilike("NationalId", "%" + filter + "%")
                                )
                        )

                ))
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("TimeOut",cc.TimeOut);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            anc.add(row);
        }
        return ok(result);
    }








}








