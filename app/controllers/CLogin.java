package controllers;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.MConfig_table;
import models.MEmployees;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;

import scala.Array;
import views.html.Login.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import views.html.Employees.*;

//import org.apache.commons.mail.*;

import static controllers.CUsers.isLoggedIn;


public class CLogin extends Controller {
    public static String response="";

    public static Result RenderLoginInterface() {
        System.out.println("the date is : " +MobileApi.getDate());
        if(MUsers.authenticateWebUser("Admin",Encryption.encryptString("1234"))==null) {
            MUsers mUsers = new MUsers();
            mUsers.UserName = "Admin";
            mUsers.Password = Encryption.encryptString("1234");
            mUsers.UserType = "web";
            mUsers.isActive = "1";
            mUsers.save();
        }
        if(MUsers.authenticateSuperAdmin("superAdmin",Encryption.encryptString("super1234"))==null){
            MUsers mUsers = new MUsers();
            mUsers.UserName = "superAdmin";
            mUsers.Password = Encryption.encryptString("super1234");
            mUsers.UserType = "superAdmin";
            mUsers.isActive = "1";
            mUsers.save();

        }

        return ok(LoginInterface.render("LoginInterface"));
    }

    public static Result getConfig(){
        System.out.println("data sent");
        String config="";
        if(MConfig_table.findConfig("Using QR Codes")!=null){
            config="Using QR Codes";
        }

        else if(MConfig_table.findConfig("Using Cards")!=null){
            config="Using Cards";
        }

        else {
            config="Not Configured" ;
        }
        return ok(config);
    }

    public static String returnConfig(){
        System.out.println("data sent");
        String config="";
        if(MConfig_table.findConfig("Using QR Codes")!=null){
            config="Using QR Codes";
        }
        else if(MConfig_table.findConfig("Using Cards")!=null){
            config="Using Cards";
        }
        else {
            config="Not Configured" ;
        }
        return config;

    }


    public static Result RenderConfig() {

        MUsers admin = isSuperAdminLoggedIn();
        if (admin == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        } else {
            System.out.println("the aid of logged in user is: "+admin.UserName);
            String config = "";
            if (MConfig_table.findConfig("Using QR Codes") != null) {
                config = "Using QR Codes";
            } else if (MConfig_table.findConfig("Using Cards") != null) {
                config = "Using Cards";
            } else {
                config = "Not Configured";
            }
            return ok(configFile.render("Configuration page", config));
        }
    }
    public static Result addConfigure(){
        DynamicForm dynamicForm=Form.form().bindFromRequest();
        String config=dynamicForm.get("systemType");
        String existing=dynamicForm.get("config");
        if(MConfig_table.findAll().size()==0)
        {
            MConfig_table mConfig_table= new MConfig_table();
            mConfig_table.systemType=config;
            mConfig_table.save();
            flash("typesuccess", "The system is configured successfully");
            return redirect(routes.CLogin.RenderLoginInterface());


        }
        else {

            if(MConfig_table.findConfig(existing)!=null) {
                MConfig_table mConfig_table2 = new MConfig_table();
                Long aid = MConfig_table.findConfig(existing).aid;
                mConfig_table2.systemType = config;
                mConfig_table2.update(aid);
                flash("typesuccess", "The system  configuration changed successfully");
                return redirect(routes.CLogin.RenderLoginInterface());
            }
            else
            {
                flash("typeerror", "The system  configuration not successful,please login first");
                return redirect(routes.CLogin.RenderLoginInterface());
            }

        }

    }


  /*public static void sendMail(String emails,String Subject,String message){
        try {
            org.apache.commons.mail.Email email = new org.apache.commons.mail.SimpleEmail();
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator("compulynxvisitors", "visitors1234"));
            email.setSSLOnConnect(true);
            email.setFrom("compulynxvisitors@gmail.com");
            email.setSubject(Subject);
            email.setMsg(message);
            email.addTo(emails);
            email.send();

            response="200";
        }catch(Exception e){

            System.out.println("email not sent");
            System.out.println(e);
            response="201";
        }
  }*/



    public static Result authenticateLogin() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String UserName = requestform.get("UserName");
        String Password = requestform.get("Password");
        System.out.println("the entered is " +UserName);
        System.out.println(MobileApi.getTime());

        if (MUsers.authenticateWebUser(UserName, Encryption.encryptString(Password)) != null)  {
            if(MConfig_table.findAll().size()==0)
            {
                return ok(configFile.render("config interface",returnConfig()));
            }

            else{
                session().clear();
                setSessions(MUsers.authenticateWebUser(UserName,Encryption.encryptString(Password)));
                System.out.println("session set");
                return redirect(routes.CDashboard.dashboardRender());
            }

        }

        //employees with password set
        else if(MEmployees.passwordAuthentication(UserName,Encryption.encryptString(Password))!=null){
                session().clear();
                setSessionsEmployee(MEmployees.passwordAuthentication(UserName,Encryption.encryptString(Password)));
                System.out.println("The employee session has been set");
                return redirect(routes.CDashboard.EmployeedashboardRender());

        }


        else if(MEmployees.authenticateEmployee(UserName,Password)!=null){

            if(MEmployees.authenticateEmployee(UserName,Password).isActive.equals("1")) {

                //password not set
                if((MEmployees.authenticateEmployee(UserName,Password).password_set==null || MEmployees.authenticateEmployee(UserName,Password).password_set.equals("") )) {
                    Long aid = MEmployees.authenticateEmployee(UserName, Password).aid;
                    System.out.println("password not set,,set password page the aid is : "+aid);
                    return ok(setPassword.render("set Password",aid));
                }
                else{
                    flash("typeerror", "You already set your password ,use your email and the set password to login");
                    return redirect(routes.CLogin.RenderLoginInterface());
                }
                /*session().clear();
                setSessionsEmployee(MEmployees.authenticateEmployee(UserName, Password));
                System.out.println("The employee session has ben set");
                return redirect(routes.CDashboard.EmployeedashboardRender());*/
            }
            else
            {
                flash("typeerror", "This employee is Deactivated!!!!");
                return redirect(routes.CLogin.RenderLoginInterface());
            }
        }
        else if(MUsers.authenticateEventWebUser(UserName,Encryption.encryptString(Password))!=null){

            session().clear();
            setSessionsEvent(MUsers.authenticateEventWebUser(UserName,Encryption.encryptString(Password)));
            System.out.println("The event user session has ben set");
            return redirect(routes.CDashboard.EventdashboardRender());
        }

   //super admin
else if(MUsers.authenticateSuperAdmin(UserName,Encryption.encryptString(Password))!=null)
        {
            session().clear();
            setSessionsSuperAdmin(MUsers.authenticateSuperAdmin(UserName,Encryption.encryptString(Password)));
            System.out.println("The super admin user session has been set");
            return redirect(routes.CLogin.RenderConfig());
        }
        else {
            flash("typeerror", "wrong UserName or Password");
            return redirect(routes.CLogin.RenderLoginInterface());
        }
    }




    public static Result logOut(){
        System.out.println("Date and time in Madrid: " + MobileApi.getTime());
        session().clear();
        return redirect(routes.CLogin.RenderLoginInterface());
    }

    private static void setSessions(MUsers userMst)
    {
        session().clear();
        session("aid",String.valueOf(userMst.aid));

    }

    public static void setSessionsEmployee(MEmployees emp)
    {
        session().clear();
        session("aid",String.valueOf(emp.aid));
    }

    private static void setSessionsEvent(MUsers event)
    {
        session().clear();
        session("aid",String.valueOf(event.aid));
    }

    private static void setSessionsSuperAdmin(MUsers superadmin)
    {
        session().clear();
        session("aid",String.valueOf(superadmin.aid));
    }

    /*public static Result forgotYourPassword() {
        ObjectNode objectNode;
        objectNode = Json.newObject();

        DynamicForm f=Form.form().bindFromRequest();
        String email=f.get("email");
        String userType=f.get("userType");

        if(MUsers.CheckPassword(email,userType)!=null){
            String password=MUsers.CheckPassword(email,userType).Password;
            String username=MUsers.CheckPassword(email,userType).UserName;

            sendMail(email,"Forgot your password","your username is "+username+" and your password is  "+password);
            System.out.println("the response is: "+Mailer.response);
            if(response.equals("200"))
            {
                System.out.println("sent successfully");
                flash("typesuccess", "your password has been sent to your email");
                return redirect(routes.CLogin.RenderLoginInterface());

            }
            else
            {
                System.out.println("not sent ");
                flash("typeerror", "oops your password not sent,server error");
                return redirect(routes.CLogin.RenderLoginInterface());

            }
        }
        else
        {
            System.out.println("no such user or the user may be inactive");
            flash("typeerror", "invalid email,your account may be inactive or you chose wrong user type");
            return redirect(routes.CLogin.RenderLoginInterface());

        }




    }*/


    public static MUsers isSuperAdminLoggedIn() {
        String aid = session("aid");
        //Long Aid=Long.parseLong(aid);
        try {
            MUsers user = MUsers.findSuperAdminUserByAid(Long.parseLong(aid));
            return user;
        }catch(Exception e){
            return null;
        }
    }


}