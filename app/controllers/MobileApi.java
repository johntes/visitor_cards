package controllers;
import models.*;
import play.*;
import play.mvc.*;
import play.mvc.Result;
import play.libs.Json;
import play.mvc.Controller;
//import org.Codehaus.jackson.node.ObjectNode;
import  com.avaje.ebean.Ebean;
import  com.avaje.ebean.Expr;
import  com.avaje.ebean.Page;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.Logger;
//import play.DynamicForm;
import play.data.Form;

import play.data.DynamicForm;

import java.util.ArrayList;
import java.util.List;
//import com.innovatrics.mrz.types.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

//import static controllers.CUsers.isLoggedIn;

public class MobileApi extends Controller {

    private static void setSessions(MUsers userMst)
    {
        session().clear();
        session("aid",String.valueOf(userMst.aid));


    }



    public static MUsers isLoggedIn() {
        String aid = session("aid");
        try {
            MUsers user = MUsers.findUsers.byId(Long.parseLong(aid));
            return user;
        }catch(Exception e){
            return null;
        }

    }


public  static Result logOutMobile(){
    ObjectNode objectNode;
    objectNode = Json.newObject();
    MUsers user = isLoggedIn();
        if(user!=null){
            session().clear();
            objectNode.put("responseCode","200");
            return  ok(objectNode);
        }
        else
            objectNode.put("responseCode","not logged in currently");
        return  ok(objectNode);
}

public static String  getTime(){
    Date date = new Date();
    DateFormat df = new SimpleDateFormat(" HH:mm");
    df.setTimeZone(TimeZone.getTimeZone("GMT+3"));
    System.out.println("Date and time in Madrid: " + df.format(date));
    return df.format(date);
}

    public static String  getDate(){
        Date date = new Date();
        DateFormat df = new SimpleDateFormat(" yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        System.out.println("Date and time in kenya is : " + df.format(date));
        return df.format(date);
    }



    public static Result authenticateMobileUsers() {
        System.out.println("Reaching the server");
        DynamicForm requestform = Form.form().bindFromRequest();
        String Usernem = requestform.get("username");
        String Password = requestform.get("pass");

        System.out.println("username: "+Usernem);
        System.out.println("Password: "+Password);

        ObjectNode objectNode;
        objectNode = Json.newObject();

        MUsers mUsers=MUsers.authenticateMobileUser(Usernem, Encryption.encryptString(Password));

        if (mUsers != null) {
            session().clear();
            setSessions(MUsers.authenticateMobileUser(Usernem,Encryption.encryptString(Password)));
            System.out.println("session set");
            //ession
            String conf=CLogin.returnConfig();
            System.out.println("the configuration is :"+conf);
            objectNode.put("conf",conf);
            objectNode.put("responseCode", "200");

        } else {
            objectNode.put("responseCode", "201");

        }
        return ok(objectNode);
    }

    public static Result ReceiveProfileUpdate() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String Usernem = requestform.get("USERNAME");
        System.out.println(Usernem);
        String Password = requestform.get("PASSWORD");
        System.out.println(Password);
        ObjectNode objectNode;
        objectNode = Json.newObject();
        String IdNumber=MUsers.authenticateEditProfile(Usernem, Password).EmploymentNumber;
        MUsers mUsers=MUsers.findUserById(IdNumber);
        mUsers.UserName=Usernem;
        mUsers.Password=Password;
        mUsers.update();
        objectNode.put("responseCode", "200");
        return ok(objectNode);
    }


    public static Result ReceiveVisitorsDetails() {
        System.out.println("HERE.....");
        ObjectNode objectNode;
        objectNode = Json.newObject();
        MVisitors mVisitors = new MVisitors();
        MVisitorRecords mVisitorRecords = new MVisitorRecords();
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("Fullname");
        System.out.println("Fullname:  " + FullName);
        String Email = requestform.get("Email");
        System.out.println("Email: " + Email);
        String SelectedCountry = requestform.get("SelectedCountry");
        System.out.println("SelectedCountry: " + SelectedCountry);
        String Id = requestform.get("Id");
        String Company = requestform.get("Company");
        String Visittype = requestform.get("VisitType");
        String Host = requestform.get("Host");
        String TimeIn = requestform.get("TimeIn");
        System.out.println("TimeIn:" + TimeIn);
        String Languages = requestform.get("Language");
        String TimeOut = requestform.get("TimeOut");
        String Gender = requestform.get("Gender");
        String Image = requestform.get("base64image");
        System.out.println("Image:" + Image);
        String Date = requestform.get("Date");
        String cardNumber = requestform.get("cardNumber");
        String empAid=requestform.get("empAid");
        //System.out.println("card number:" + cardNumber);
        String phone = requestform.get("phoneNumber");
        System.out.println("phone Number is:" + phone);

        String generated_by=requestform.get("generated_by");
        String QrImage=requestform.get("QrImage");

        System.out.println("the aid of the employee is :"+empAid);

        String useCards = "Using Cards";
        String useQr = "Using QR Codes";
        String conf = CLogin.returnConfig();



//cards
        if (conf.equals(useCards)) {
            System.out.println("using cards");

            if(MCards.findUnavailableCard(cardNumber)!=null)
            {
                objectNode.put("responseCode","202");
                return ok(objectNode);

            }
            else if(MCards.findAvailableCard(cardNumber)==null){
                System.out.println("no such card");
                objectNode.put("responseCode","201");
                return ok(objectNode);

            }
            //check if the visitor is already in .if available return response code 203
            else if(MVisitors.checkVisitorStatus(Id)!=null){
                System.out.println("this visitor is currently in");
                objectNode.put("responseCode","203");
                return ok(objectNode);

            }

            else
            if(MVisitorRecords.findVisitorById(Id)!=null) {

                System.out.println("visitor available in our db:");
                System.out.println("id is:" + Id);
                long aid2 = MVisitorRecords.findVisitorById(Id).aid;
                System.out.println("aid is:" + aid2);

                int numOfVisits = MVisitorRecords.findVisitorById(Id).NoOfTimes;
                System.out.println("aid is:" + numOfVisits);


                //if email and image was null(not available)
                if(mVisitorRecords.Email==null || mVisitorRecords.Email.equals(""))
                {
                    mVisitorRecords.Email=Email;
                }

                if(mVisitorRecords.Image==null || mVisitorRecords.Image.equals(""))
                {
                    mVisitorRecords.Image=Image;
                }

                //UPDATING TABLE VISITOR RECORD TO INCREASE NUMBER OF ATTENDANCE TIMES AND CREATING A NEW RECORD
                mVisitorRecords.NoOfTimes = numOfVisits+1;
                mVisitorRecords.update(aid2);
            }

            else
            {
                System.out.println("visitor not available on our database");
                MVisitorRecords mVisitorRecords1=new MVisitorRecords();
                mVisitorRecords1.FullName = FullName;
                mVisitorRecords1.Email = Email;
                mVisitorRecords1.Country = SelectedCountry;
                mVisitorRecords1.NationalId = Id;
                mVisitorRecords1.Company = Company;
                mVisitorRecords1.Languages = Languages;
                mVisitorRecords1.Gender = Gender;
                mVisitorRecords1.NoOfTimes=1;
                mVisitorRecords1.phoneNumber=phone;
                mVisitorRecords1.Image = Image;
                System.out.println("1 the phone number inserted is:"+phone);
                mVisitorRecords1.save();

            }
//change id and card
            mVisitors.FullName = FullName;
            mVisitors.Email = Email;
            mVisitors.Country = SelectedCountry;
            mVisitors.NationalId = Id;
            mVisitors.Company = Company;
            mVisitors.TimeIn = getTime();
            mVisitors.Host = Host;
            mVisitors.Languages = Languages;
            mVisitors.TimeOut = "";
            mVisitors.Type = Visittype;
            mVisitors.Gender = Gender;
            mVisitors.Date = getDate();
            mVisitors.Image = Image;
            mVisitors.isActive = "1";
            mVisitors.isDeleted="1";
            mVisitors.cardNumber = cardNumber;
            mVisitors.phoneNumber=phone;
            mVisitors.employeeAid=empAid;
            System.out.println("1 the phone number inserted is:"+phone);

            mVisitors.save();

            MCards mCards=new MCards();
            long Aid=MCards.exitCard(cardNumber).aid;
            mCards.isActive="1";
            mCards.update(Aid);

            if(MExpectedVisitors.findActiveVisitorById(Id)!=null)
            {
                MExpectedVisitors mExpectedVisitors3=MExpectedVisitors.findActiveVisitorById(Id);
                long aid=mExpectedVisitors3.aid;
                mExpectedVisitors3.isActive="0";
                mExpectedVisitors3.update(aid);
            }

            //sending email to employee
if(MEmployees.findEmployeeByAid(empAid)!=null)
{
    String email=MEmployees.findEmployeeByAid(empAid).Email;
    String empName=MEmployees.findEmployeeByAid(empAid).FullName;
   // CLogin.sendMail(email,"You have a Visitor ","hey  "+empName+" your visitor "+FullName+" has arrived at  "+getTime()+" for  "+Visittype+"purpose");
    System.out.println("the response is: "+CLogin.response);
    if(CLogin.response.equals(""))
    {
        System.out.println("checked in visitor and mail sent successfully");
        objectNode.put("responseCode", "200");
        return ok(objectNode);
    }


}
else{
    System.out.println("checked in visitor but mail not sent");
    objectNode.put("responseCode", "204");
    return ok(objectNode);
}



        }

        //using qr codes
        else if(conf.equals(useQr)){
            if(MVisitors.checkVisitorStatus(Id)!=null){
                objectNode.put("responseCode","203");
                return ok(objectNode);
            }

            else
            if(MVisitorRecords.findVisitorById(Id)!=null) {

                System.out.println("id is:" + Id);
                long aid2 = MVisitorRecords.findVisitorById(Id).aid;
                System.out.println("aid is:" + aid2);
                int numOfVisits = MVisitorRecords.findVisitorById(Id).NoOfTimes;
                System.out.println("aid is:" + numOfVisits);
                //UPDATING TABLE VISITOR RECORD TO INCREASE NUMBER OF ATTENDANCE TIMES AND CREATING A NEW RECORD
                mVisitorRecords.NoOfTimes = numOfVisits+1;
                mVisitorRecords.update(aid2);
            }


            else
            {
                int noOfTimes=1;
                MVisitorRecords mVisitorRecords1=new MVisitorRecords();
                mVisitorRecords1.FullName = FullName;
                mVisitorRecords1.Email = Email;
                mVisitorRecords1.Country = SelectedCountry;
                mVisitorRecords1.NationalId = Id;
                mVisitorRecords1.Company = Company;
                mVisitorRecords1.Languages = Languages;
                mVisitorRecords1.Gender = Gender;
                mVisitorRecords1.phoneNumber=phone;
                mVisitorRecords1.NoOfTimes=noOfTimes;
                mVisitorRecords1.Image = Image;
                mVisitorRecords1.save();

            }
            mVisitors.FullName = FullName;
            mVisitors.Email = Email;
            mVisitors.Country = SelectedCountry;
            mVisitors.NationalId = Id;
            mVisitors.Company = Company;
            mVisitors.TimeIn = getTime();
            mVisitors.Host = Host;
            mVisitors.Languages = Languages;
            mVisitors.TimeOut = "";
            mVisitors.Type = Visittype;
            mVisitors.Gender = Gender;
            mVisitors.Date = getDate();
            mVisitors.Image = Image;
            mVisitors.isActive = "1";
            // mVisitors.cardNumber = cardNumber;
            mVisitors.phoneNumber=phone;
            mVisitors.QrImage=QrImage;
            mVisitors.isDeleted="0";
            mVisitors.employeeAid=empAid;
            mVisitors.save();


            MQR mqr=new MQR();
            mqr.isActive="1";
            mqr.time_generated=getTime();
            mqr.generated_by=generated_by;
            mqr.VisitorId=Id;
            mqr.QrImage=QrImage;
            mqr.save();


            if(MExpectedVisitors.findActiveVisitorById(Id)!=null)
            {
                MExpectedVisitors mExpectedVisitors3=MExpectedVisitors.findActiveVisitorById(Id);
                long aid=mExpectedVisitors3.aid;
                mExpectedVisitors3.isActive="0";
                mExpectedVisitors3.update(aid);
            }

            //sending mail to the employee

            if(MEmployees.findEmployeeByAid(empAid)!=null)
            {
                String email=MEmployees.findEmployeeByAid(empAid).Email;
                String empName=MEmployees.findEmployeeByAid(empAid).FullName;
                //CLogin.sendMail(email,"You have a Visitor ","hey  "+empName+" your visitor "+FullName+" has arrived at  "+getTime()+" for  "+Visittype+"purpose");
                System.out.println("the response is: "+CLogin.response);
                if(CLogin.response.equals(""))
                {
                    System.out.println("checked in visitor and mail sent successfully");
                    objectNode.put("responseCode", "200");
                    return ok(objectNode);
                }
                else{
                    System.out.println("checked in visitor but mail not sent");
                    objectNode.put("responseCode", "204");
                    return ok(objectNode);
                }

            }
            else{
                System.out.println("checked in visitor but mail not sent");
                objectNode.put("responseCode", "204");
                return ok(objectNode);
            }

        }

        return ok(objectNode);
    }


    public static Result ReceiveExitingVisitors(){
        System.out.println("HERE.....");
        ObjectNode objectNode;
        objectNode = Json.newObject();
        MUsers user = isLoggedIn();
        System.out.println("HERE.....");

        String useCards = "Using Cards";
        String useQr = "Using QR Codes";
        String conf = CLogin.returnConfig();
        if(conf.equals(useCards))
        {
            DynamicForm requestform = Form.form().bindFromRequest();
            String cardNumber = requestform.get("cardNumber");
            String TimeOut = requestform.get("TimeOut");
            System.out.println("cardNumber:" + cardNumber);
            MVisitors mVisitors = new MVisitors();
            MCards mCards=new MCards();
            if (MVisitors.checkVisitor(cardNumber, "1") != null) {
                long id = MVisitors.checkVisitor(cardNumber, "1").aid;
                mVisitors.isActive = "0";
                mVisitors.TimeOut = TimeOut;
                mVisitors.update(id);

                if(MCards.exitCard(cardNumber)!=null){
                    long Aid=MCards.exitCard(cardNumber).aid;
                    mCards.isActive="0";
                    mCards.update(Aid);
                    objectNode.put("responseCode", "200");
                }
            } else {
                objectNode.put("responseCode", "201");
            }
        }

        else if(conf.equals(useQr)){
            DynamicForm requestform = Form.form().bindFromRequest();
            String VisitorId = requestform.get("cardNumber");
            System.out.println("request is comming");
           // String TimeOut = requestform.get("TimeOut");
            System.out.println("id number:" + VisitorId);
            MVisitors mVisitors = new MVisitors();
            //MCards mCards=new MCards();


            if (MVisitors.checkVisitorQr(VisitorId) != null) {
                long id = MVisitors.checkVisitorQr(VisitorId).aid;
                mVisitors.isActive = "0";
                mVisitors.TimeOut = getTime();
                mVisitors.update(id);

                MQR mqr=new MQR();
                if(MQR.exitQr(VisitorId)!=null){
                    long Aid=MQR.exitQr(VisitorId).aid;
                    mqr.isActive="0";
                    mqr.update(Aid);
                    objectNode.put("responseCode", "200");
                    return ok(objectNode);

                }

            } else {
                //qr code does not exist or is not active
                objectNode.put("responseCode", "201");
                return ok(objectNode);
            }
        }
            return ok(objectNode);
    }


//GET ALREADY EXISTING VISITOR DETAILS
    public static Result ReturnVisitorsDetails(){
        System.out.println("Getting visitor details");
        ObjectNode result;
        DynamicForm form=Form.form().bindFromRequest();
        String ID=form.get("IdNumber");
        result = Json.newObject();

        if(MVisitors.checkVisitorStatus(ID)!=null)
        {
            result.put("responseCode","202");
            return  ok(result);
        }

      else  if(MExpectedVisitors.findActiveVisitorById(ID)!=null){

            String FullName = MExpectedVisitors.findActiveVisitorById(ID).FullName;
            String Email = MExpectedVisitors.findActiveVisitorById(ID).Email;
            String SelectedCountry = MExpectedVisitors.findActiveVisitorById(ID).Country;
            String Id = MExpectedVisitors.findActiveVisitorById(ID).NationalId;
            String Company = MExpectedVisitors.findActiveVisitorById(ID).Company;
            String Languages = MExpectedVisitors.findActiveVisitorById(ID).Languages;
            String Gender = MExpectedVisitors.findActiveVisitorById(ID).Gender;
            String Host = MExpectedVisitors.findActiveVisitorById(ID).HostName;
            String phoneNumber = MExpectedVisitors.findActiveVisitorById(ID).phoneNumber;

            result.put("responseCode","200");
            result.put("FullName",FullName);
            result.put("Email",Email);
            result.put("Country",SelectedCountry);
            result.put("NationalId",Id);
            result.put("Company",Company);
            result.put("Gender",Gender);
            result.put("Languages",Languages);
            result.put("phoneNumber",phoneNumber);
            result.put("Host",Host);

            return  ok(result);
        }



      else  if(MVisitorRecords.findVisitorById(ID)!=null){

            String FullName = MVisitorRecords.findVisitorById(ID).FullName;
            String Email = MVisitorRecords.findVisitorById(ID).Email;
            String SelectedCountry = MVisitorRecords.findVisitorById(ID).Country;
            String Id = MVisitorRecords.findVisitorById(ID).NationalId;
            String Company = MVisitorRecords.findVisitorById(ID).Company;
            String Languages = MVisitorRecords.findVisitorById(ID).Languages;
            String Gender = MVisitorRecords.findVisitorById(ID).Gender;
            String phoneNumber = MVisitorRecords.findVisitorById(ID).phoneNumber;
            String image=MVisitorRecords.findVisitorById(ID).Image;

            result.put("responseCode","200");
            result.put("FullName",FullName);
            result.put("Email",Email);
            result.put("Country",SelectedCountry);
            result.put("NationalId",Id);
            result.put("Company",Company);
            result.put("Gender",Gender);
            result.put("Languages",Languages);
            result.put("phoneNumber",phoneNumber);
            result.put("image",image);


            return  ok(result);

        }

        else
        result.put("responseCode","201");
        return ok(result);
    }



    public static Result return_users() {
        System.out.println("Reaching on the server");
        return ok(Json.toJson(MUsers.findAll()));
    }

    public static Result forgotPassword() {
        ObjectNode objectNode;
        objectNode = Json.newObject();

        DynamicForm f=Form.form().bindFromRequest();
       String email=f.get("email");
       String userType=f.get("userType");

        System.out.println("got all your details----------------------------------"+email+userType);

       if(MUsers.CheckPassword(email,userType)!=null){
           String password=MUsers.CheckPassword(email,userType).Password;
           String username=MUsers.CheckPassword(email,userType).UserName;

           //CLogin.sendMail(email,"Visitors management  Password","The username is : "+username +" and your password is "+password);

          // Mailer.send("compulynxvisitors@gmail.com","visitors1234",email,"Forgot your password","your username is "+username+" and your password is  "+password);
           System.out.println("the response is: "+CLogin.response);
           if(CLogin.response.equals(""))
           {
               objectNode.put("responseCode","200");
               System.out.println("sent successfully");
           }
           else
           {
               objectNode.put("responseCode","201");
               System.out.println("not sent ");
           }


       }
       else
       {
           objectNode.put("responseCode","202");
           System.out.println("no such user or the user may be inactive");
       }


        return ok(objectNode);

    }


    public static Result return_current_visitors() {
        System.out.println("Reaching on the server");
        return ok(Json.toJson(MVisitors.findCurrentVisitors()));
    }

    public static Result return_exited_visitors() {
        System.out.println("Reaching on the server");
        return ok(Json.toJson(MVisitors.findExitedVisitors()));
    }

    public static Result return_expected_visitors() {
        System.out.println("Reaching on the server");
        return ok(Json.toJson(MExpectedVisitors.findActiveExpctedVisitorList()));
    }



    //return current visitots in the company
    public static Result return_Visitor() {
        ObjectNode objectNode;
        objectNode = Json.newObject();
        System.out.println("Reaching on the server");
        System.out.println("HERE.....");
            int current = MVisitors.findActiveVisitors().size();
            int total = MVisitors.findAll().size();
            int exited = MVisitors.findDiActivetedVisitors().size();
           int expected=MExpectedVisitors.findActiveExpctedVisitorList().size();
            System.out.println("the size of currrent visitors is: " + current);
            System.out.println("the size total visitors is: " + total);
            System.out.println("the size exited visitors is: " + exited);
            objectNode.put("current", current);
            objectNode.put("total", total);
            objectNode.put("exited", exited);
            objectNode.put("expected", expected);
            return ok(objectNode);
    }



    //RETURN EXITED VISITORS
    public static Result return_exitedVisitor() {
        ObjectNode objectNode;
        objectNode = Json.newObject();
        System.out.println("Reaching on the server");
        int sized=MVisitors.findDiActivetedVisitors().size();
        System.out.println("the size of all active visitors is: "+sized);
        objectNode.put("responseCode", sized);
        return ok(objectNode);
    }

    //return total visitots in the company
    public static Result return_totalVisitors(){
        ObjectNode objectNode;
        objectNode = Json.newObject();
        System.out.println("Reaching  on the server");
        int sized=MVisitors.findAll().size();
        System.out.println("the size of all the visitors is: "+sized);
        objectNode.put("responseCode", sized);
        return ok(objectNode);
    }

//RETURN EMPLOYEE LIST
    public static Result return_employees(){
        return ok(Json.toJson(MEmployees.findActiveEmployees()));
    }


    public static Result check_card(){
        DynamicForm form=Form.form().bindFromRequest();
        String card_number=form.get("card_number");
        ObjectNode objectNode=Json.newObject();

        if(MCards.findUnavailableCard(card_number)!=null)
        {
            objectNode.put("responseCode","card is in use");
            return ok(objectNode);
        }
       else if(MCards.findAvailableCard(card_number)==null){
            objectNode.put("responseCode","no such card");
            return ok(objectNode);
        }

        else
            objectNode.put("responseCode","200");
        return ok(objectNode);

    }


/*public static Result sendText(){

    String message="&message=" +"you have a visitor at the recemption";
    String numbers="&to=" +0704473353;

    URL obj = new URL("https://api.africastalking.com/restless/send?username=patamech&Apikey=c84a9dc5a1b99e23326da463cab9737ff5c9bd317d0c92561075712fdf2aaf60" + numbers + message);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    con.setRequestMethod("GET");
    int responseCode = con.getResponseCode();
    System.out.println("GET Response Code :: " + responseCode);

    ObjectNode objectNode=Json.newObject();
    objectNode.put("responseCode","200");
        return ok(objectNode);
}*/

public static Result editVisitor(){
    DynamicForm d=Form.form().bindFromRequest();

    System.out.println("edit request reached");
    String idNumber="";
    String aid=d.get("aid");
    String email=d.get("email");
    String phone=d.get("phone");
    String name=d.get("name");
    String company=d.get("company");
    idNumber=d.get("idNumber");
    String host=d.get("host");

    System.out.println("email " +email);
    System.out.println("phone " +phone);
    System.out.println("name " +name);
    System.out.println("company " +company);
    System.out.println("idNumber " +idNumber);
    System.out.println("host " +host);

    ObjectNode objectNode=Json.newObject();
    int debug=0;

    try {
        if(MVisitors.findVisitorById(idNumber)==null) {
            objectNode.put("responseCode", "201");
        }

        else if(MVisitorRecords.findVisitorById(idNumber)==null)
        {
            objectNode.put("responseCode", "201");
        }

        else{

            Long Aid = MVisitors.findVisitorById(idNumber).aid;
            MVisitors mVisitors = new MVisitors();
            mVisitors.FullName = name;
            mVisitors.phoneNumber = phone;
            mVisitors.Company = company;
            mVisitors.Email = email;
            mVisitors.Host=host;
            mVisitors.update(Aid);
            System.out.println("success");


            Long Aid2=MVisitorRecords.findVisitorById(idNumber).aid;
            MVisitorRecords mVisitorRecords=new MVisitorRecords();
            mVisitorRecords.phoneNumber=phone;
            mVisitorRecords.FullName=name;
            mVisitorRecords.Company=company;
            mVisitorRecords.Email=email;
            mVisitorRecords.update(Aid2);
              debug=1;
            objectNode.put("responseCode", "200");
        }

            }catch(Exception e)
            {
                System.out.println("record not updated");
                objectNode.put("responseCode","201");
            }


    return ok(objectNode);
}


}

