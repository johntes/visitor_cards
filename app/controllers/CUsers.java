package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.MEmployees;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;

import java.security.GeneralSecurityException;
import java.util.Map;
import views.html.Users.*;

import static controllers.EncryptionTest.decrypt;
import static controllers.EncryptionTest.encrypt;

public class CUsers extends Controller{
  //static   String encryptedPasswor="";


    public static MUsers isLoggedIn() {
        String aid = session("aid");
        try {
            MUsers user = MUsers.findNormalWebUserByAid(Long.parseLong(aid));
            return user;
        }catch(Exception e){
            return null;
        }

    }



    public static Result RenderAddUser() {

        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(AddUsers.render("Add User"));
    }


    public static Result RenderViewUsers() {

        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));


        return ok(ViewUsers.render("View User")); }


    public static Result ActivateUser(Long id){
        MUsers mUsers = new MUsers();
        mUsers.aid=id;
        mUsers.isActive="1";
        mUsers.update();

        return redirect(routes.CUsers.RenderViewUsers());
    }
    public static Result DeActivateUser(Long id){
        MUsers mUsers = new MUsers();
        mUsers.aid=id;
        mUsers.isActive="0";
        mUsers.update();

        return redirect(routes.CUsers.RenderViewUsers());
    }


    public static Result ReceiveUsers() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String UserName = requestform.get("UserName");
        String Email = requestform.get("Email");
        String Password = requestform.get("Password");
        String  ConfirmPassword=requestform.get("ConfirmPassword");
        String EmploymentNumber = requestform.get("EmploymentNumber");
        String User = requestform.get("User");


        System.out.println("UserName: "+UserName);
        System.out.println("Email: "+Email);
        System.out.println("Password: "+Password);
        System.out.println("ConfirmPassword: "+ConfirmPassword);
        System.out.println("User: "+User);
        System.out.println("EmploymentNumber: "+EmploymentNumber);
        return ok();

    }
    public static Result AddUser() {
        String encryptedPassword="";
        DynamicForm requestform = Form.form().bindFromRequest();
        String UserName = requestform.get("UserName");
        String Email = requestform.get("Email");
        String Password = requestform.get("Password");
        encryptedPassword=Encryption.encryptString(Password);


        String User = requestform.get("UserType");
        String ConfirmPassword = requestform.get("ConfirmPassword");
        String EmploymentNumber = requestform.get("EmploymentNumber");
        System.out.println("User Type"+User);

        if(!(EmploymentNumber.equals(""))) {
            if (MEmployees.findActiveEmployee(EmploymentNumber) == null) {
                flash("typeerror", "error,no such employee number in our records,not added");
                return redirect(routes.CUsers.RenderViewUsers());
            }
        }

       else if(MUsers.findUserNameById(UserName)!=null){
            flash("typeerror", "error, user with that username  already exist");
            return redirect(routes.CUsers.RenderViewUsers());
        }

        else if(MUsers.CheckPassword(Email,User)!=null){
            flash("typeerror", " error,that email has been used to register the same usertype");
            return redirect(routes.CUsers.RenderViewUsers());
        }


        if (Password.equals(ConfirmPassword)) {






            System.out.println("Matched");
            MUsers mUsers = new MUsers();
            mUsers.UserName = UserName;
            mUsers.Email = Email;
            mUsers.EmploymentNumber = EmploymentNumber;
            mUsers.Password = encryptedPassword;
            mUsers.ConfirmPassword = encryptedPassword;
            mUsers.UserType=User;
            mUsers.isActive = "1";
            mUsers.save();
            flash("success", "user added successfully");
            return redirect(routes.CUsers.RenderViewUsers());

        } else {
            System.out.println("Not Matched");
            flash("typeerror", "Password does not match");
            return redirect(routes.CUsers.RenderAddUser());
        }

    }


        public static Result userList () {
            Map<String, String[]> params = request().queryString();

            Integer iTotalRecords = MUsers.findUsers.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

            /**
             * Get sorting order and column
             */
            String sortBy = "Email";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "Email";
                    break;
                case 1:
                    sortBy = "aid";
                    break;
                case 2:
                    sortBy = "EmploymentNumber";
                    break;
            }

            Page<MUsers> areaPage = MUsers.findUsers.where(

                    Expr.or(
                            Expr.ilike("Email", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("EmploymentNumber", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();

            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MUsers cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                //    System.out.println("in data table fetch: " + cc.UserName);

                row.put("aid", cc.aid);
                row.put("UserName", cc.UserName);
                row.put("Email", cc.Email);
                row.put("EmploymentNumber", cc.EmploymentNumber);
                row.put("Password", cc.Password);
                row.put("User", cc.UserType);
                row.put("isActive", cc.isActive);

                anc.add(row);
            }

            return ok(result);

        }







    }
