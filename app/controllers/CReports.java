    package controllers;


    import play.mvc.*;
    import java.util.Map;
    import java.util.HashMap;
    import java.io.ByteArrayInputStream;
    import java.io.ByteArrayOutputStream;
    import java.io.InputStream;
    import java.io.OutputStream;

    import net.sf.jasperreports.engine.JasperFillManager;
    import net.sf.jasperreports.engine.JasperPrint;
    import net.sf.jasperreports.engine.export.JExcelApiExporter;
    import net.sf.jasperreports.engine.JRExporter;
    import net.sf.jasperreports.engine.export.JRPdfExporter;
    import net.sf.jasperreports.engine.JRExporterParameter;
    import net.sf.jasperreports.engine.JasperCompileManager;
    import org.codehaus.jackson.node.ArrayNode;
    import org.codehaus.jackson.node.ObjectNode;
    import play.mvc.Security;
    import play.Logger;
    import play.cache.Cache;
    import play.data.Form;
    import play.mvc.Controller;
    import play.mvc.Result;
    import models.*;
    import views.html.report.exitedVisitorsReport;

    import static controllers.CUsers.isLoggedIn;
    import static play.data.Form.form;
    // import com.google.gson.Gson;
    import play.libs.Json;
    import play.data.DynamicForm;
    import play.data.Form;


    public class CReports extends Controller  {
        public static InputStream generateReport(String reportDefFile, Map reportParams) {
            OutputStream os = new ByteArrayOutputStream();
            try {
                String compiledFile =  reportDefFile + ".jasper";
                JasperCompileManager.compileReportToFile(reportDefFile + ".jrxml", compiledFile);
                JasperPrint jrprint = JasperFillManager.fillReport(compiledFile, reportParams, play.db.DB.getConnection());
                //JRExporter exporter = new JExcelApiExporter();
                JRExporter exporter = null;
                exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrprint);
                exporter.exportReport();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ByteArrayInputStream(((ByteArrayOutputStream) os).toByteArray());
        }




        public static Result renderExitedVisitorsReport()
        {
            MUsers user=isLoggedIn()  ;
            if (user == null) {
                System.out.println("not logged in");
                return redirect(routes.CLogin.RenderLoginInterface());
            }
            return ok(
                    views.html.report.exitedVisitorsReport.render("views/Reports")
            );
        }
        public static Result exitedVisitors()
        {
            MUsers user = isLoggedIn();
            if (user == null) {
                System.out.println("not logged in");
                return redirect(routes.CLogin.RenderLoginInterface());
            }


            String dateFrom=Form.form().bindFromRequest().get("fromdate");
            String dateTo=Form.form().bindFromRequest().get("todate");
            System.out.println("date from"+dateFrom);
            Map	reportParams = new HashMap<String, String>();
            reportParams.put("aid", "aid");
            reportParams.put("full_name", "full_name");
            reportParams.put("email", "email");
            reportParams.put("phone_number", "phone_number");
            reportParams.put("country", "country");
            reportParams.put("national_id", "national_id");
            reportParams.put("company", "company");
            reportParams.put("type", "type");
            reportParams.put("host", "host");
            reportParams.put("time_in", "time_in");
            reportParams.put("time_out", "time_out");
            reportParams.put("date", "date");
            reportParams.put("fromdate", dateFrom);
            reportParams.put("todate", dateTo);
            return ok(generateReport("./app/views/report/exitedVisitorsLandscape", reportParams)).as("application/pdf");

        }


    }