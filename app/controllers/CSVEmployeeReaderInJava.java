package controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple Java program to read CSV file in Java. In this program we will read
 * list of books stored in CSV file as comma separated values.
 *
 * @author WINDOWS 8
 *
 */
public class CSVEmployeeReaderInJava  {



    public static List<Employee> readBooksFromCSV(File file) throws IOException {
        List<Employee> employees = new ArrayList<>();
        final BufferedReader br = new BufferedReader(new FileReader(file));


            // read the first line from the text file
            String line = br.readLine();

            // loop until all lines are read
        int iteration=0;
            while (line != null) {

                // use string.split to load a string array with the values from
                // each line of
                // the file, using a comma as the delimiter
//ignoring the first line
                if(iteration == 0) {
                    iteration++;
                    continue;
                }

                String[] attributes = line.split(",");

                Employee employee = createEmployee(attributes);

                // adding employee into ArrayList
                employees.add(employee);

                // read next line before looping
                // if end of file reached, line would be null
                line = br.readLine();
            }




        return employees;
    }

    public static Employee createEmployee(String[] metadata) {
        String  FullNames="";
        String Email="";
         String IdNumber="";
        String EmploymentNumber="";
       // String MobileNumber="";
        String Department="";


        System.out.println("the length is:"+metadata.length);
        if(metadata.length<1)
        {
         return null;
        }
       else if(metadata.length<2)
        {
            FullNames = metadata[0];
        }
       else if(metadata.length<3)
        {
            FullNames = metadata[0];
            Email = metadata[1];

        }
       else if(metadata.length<4)
        {
            FullNames = metadata[0];
            Email = metadata[1];
            IdNumber = metadata[2];
        }
       else if(metadata.length<5)
        {
            FullNames = metadata[0];
            Email = metadata[1];
            IdNumber = metadata[2];
            EmploymentNumber = metadata[3];
        }
       else if(metadata.length<6)
        {
            FullNames = metadata[0];
            Email = metadata[1];
            IdNumber = metadata[2];
            EmploymentNumber = metadata[3];
            Department = metadata[4];
            //MobileNumber = metadata[4];
        }
       else if(metadata.length<7)
        {
            FullNames = metadata[0];
            Email = metadata[1];
            IdNumber = metadata[2];
            EmploymentNumber = metadata[3];
           // MobileNumber = metadata[4];
            Department = metadata[4];
            System.out.println("emp num: >>>>>"+EmploymentNumber);
            //System.out.println("emp num: >>>>>"+MobileNumber);


        }




        // create and return book of this metadata
        return new Employee(FullNames,Email,IdNumber,EmploymentNumber,Department);
    }

}

class Employee {

     private String  FullNames;
     private String Email;
    private String IdNumber;
    private String EmploymentNumber;
    //private String MobileNumber;
    private String Department;




    public Employee(String FullNames,String Email,String IdNumber,String EmploymentNumber,String Department) {
        this.FullNames=FullNames;
        this.Email=Email;
        //this.MobileNumber=MobileNumber;
        this.EmploymentNumber=EmploymentNumber;
        this.IdNumber=IdNumber;
        this.Department=Department;
    }

    public String getFullNames() {
        return FullNames;
    }

    public void setFullNames(String fullNames) {
        FullNames = fullNames;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getIdNumber() {
        return IdNumber;
    }

    public void setIdNumber(String idNumber) {
        IdNumber = idNumber;
    }

    public String getEmploymentNumber() {
        return EmploymentNumber;
    }

    public void setEmploymentNumber(String employmentNumber) {
        EmploymentNumber = employmentNumber;
    }

   /* public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }*/

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    @Override
    public String toString() {
        return "Book [name=" + FullNames + ", index=" + Email
                + "]";
    }

}