# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table mcards (
  aid                       bigint auto_increment not null,
  card_number               varchar(255),
  is_active                 varchar(255),
  admin_activated           varchar(255),
  constraint pk_mcards primary key (aid))
;

create table mconfig_table (
  aid                       bigint auto_increment not null,
  system_type               varchar(255),
  constraint pk_mconfig_table primary key (aid))
;

create table memployees (
  aid                       bigint auto_increment not null,
  full_name                 varchar(255),
  email                     varchar(255),
  id_number                 varchar(255),
  department                varchar(255),
  employment_number         varchar(255),
  phone_number              varchar(255),
  password                  varchar(255),
  password_set              varchar(255),
  is_active                 varchar(255),
  constraint pk_memployees primary key (aid))
;

create table mevent_visitor (
  aid                       bigint auto_increment not null,
  full_name                 varchar(255),
  email                     varchar(255),
  phone_number              varchar(255),
  organisation              varchar(255),
  confirmation              varchar(255),
  designation               varchar(255),
  event_aid                 varchar(255),
  attended                  varchar(255),
  expected                  varchar(255),
  country                   varchar(255),
  national_id               varchar(255),
  address                   varchar(255),
  gender                    varchar(255),
  languages                 varchar(255),
  card_image                varchar(255),
  time_in                   varchar(255),
  constraint pk_mevent_visitor primary key (aid))
;

create table mevents (
  aid                       bigint auto_increment not null,
  event_name                varchar(255),
  event_date                varchar(255),
  event_status              varchar(255),
  event_venue               varchar(255),
  event_check_out_time      varchar(255),
  constraint pk_mevents primary key (aid))
;

create table mexpected_visitors (
  aid                       bigint auto_increment not null,
  full_name                 varchar(255),
  email                     varchar(255),
  country                   varchar(255),
  national_id               varchar(255),
  address                   varchar(255),
  company                   varchar(255),
  host_name                 varchar(255),
  gender                    varchar(255),
  languages                 varchar(255),
  phone_number              varchar(255),
  date                      varchar(255),
  visit_type                varchar(255),
  employee_id               varchar(255),
  is_active                 varchar(255),
  time                      varchar(255),
  vip                       varchar(255),
  constraint pk_mexpected_visitors primary key (aid))
;

create table mpremise (
  aid                       bigint auto_increment not null,
  premise_name              varchar(255),
  constraint pk_mpremise primary key (aid))
;

create table mqr (
  aid                       bigint auto_increment not null,
  visitor_id                varchar(255),
  is_active                 varchar(255),
  time_generated            varchar(255),
  generated_by              varchar(255),
  qr_image                  varchar(255),
  constraint pk_mqr primary key (aid))
;

create table musers (
  aid                       bigint auto_increment not null,
  user_name                 varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  user_type                 varchar(255),
  confirm_password          varchar(255),
  employment_number         varchar(255),
  is_active                 varchar(255),
  constraint pk_musers primary key (aid))
;

create table mvisitor_records (
  aid                       bigint auto_increment not null,
  full_name                 varchar(255),
  email                     varchar(255),
  country                   varchar(255),
  national_id               varchar(255),
  address                   varchar(255),
  company                   varchar(255),
  gender                    varchar(255),
  languages                 varchar(255),
  image                     varchar(255),
  phone_number              varchar(255),
  no_of_times               integer,
  constraint pk_mvisitor_records primary key (aid))
;

create table mvisitors (
  aid                       bigint auto_increment not null,
  full_name                 varchar(255),
  email                     varchar(255),
  phone_number              varchar(255),
  country                   varchar(255),
  national_id               varchar(255),
  company                   varchar(255),
  type                      varchar(255),
  host                      varchar(255),
  gender                    varchar(255),
  time_in                   varchar(255),
  time_out                  varchar(255),
  languages                 varchar(255),
  date                      varchar(255),
  image                     varchar(255),
  is_active                 varchar(255),
  card_number               varchar(255),
  is_deleted                varchar(255),
  qr_image                  varchar(255),
  employee_aid              varchar(255),
  constraint pk_mvisitors primary key (aid))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table mcards;

drop table mconfig_table;

drop table memployees;

drop table mevent_visitor;

drop table mevents;

drop table mexpected_visitors;

drop table mpremise;

drop table mqr;

drop table musers;

drop table mvisitor_records;

drop table mvisitors;

SET FOREIGN_KEY_CHECKS=1;

