import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "VisitorsManagementSystem"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
  "mysql" % "mysql-connector-java" % "5.1.18",
  "net.sourceforge.jtds" % "jtds" % "1.2",
  "commons-io" % "commons-io" % "2.4",
    "javax.activation" % "activation" % "1.1.1",
    "org.apache.commons" % "commons-email" % "1.3.1",
    "org.apache.httpcomponents" % "httpclient" % "4.0-beta1",
    "org.apache.httpcomponents" % "httpclient" % "4.5.5",
    
    "jasperreports" % "jasperreports" % "3.5.3",
    "com.lowagie" % "itext" % "2.1.7",
    "com.lowagie" % "itext" % "2.1.7",
    "net.sourceforge.jexcelapi" % "jxl" % "2.6.10",
    "org.xhtmlrenderer" % "core-renderer" % "R8",
    "net.sf.jtidy" % "jtidy" % "r938",
    "com.lowagie" % "itext-rtf" % "2.1.4",
    "org.apache.directory.studio" % "org.apache.commons.io" % "2.4",
    "commons-io" % "commons-io" % "2.4",
    "net.sf.flexjson" % "flexjson" % "2.0",
    "org.olap4j" % "olap4j" % "1.2.0",
     "ar.com.fdvs" % "DynamicJasper" % "3.0.6"
  )


 

 

  
  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
  )
  //libraryDependencies += ws

  //configuring ws lips and jwt
 /* val libraryDependencies = Seq(
    javaWs
  )*/

 /* libraryDependencies += ws
  libraryDependencies += ehcache*/
}
